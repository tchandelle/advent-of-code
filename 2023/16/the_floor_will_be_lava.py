#!/usr/bin/python3

from enum import Enum

class Dir(int, Enum):
    UP = 1
    RIGHT = 2
    DOWN = 4
    LEFT = 8

def keep_exploring(layout, pos, direction, visited):
    x, y = pos
    if y < 0 or y >= len(layout) or x < 0 or x >= len(layout[0]):
        return False
    if pos in visited and (visited[pos] & direction) > 0:
        return False
    return True

def explore(layout, pos, direction, visited):
    while keep_exploring(layout, pos, direction, visited):
        visited[pos] = visited[pos] | direction if pos in visited else direction

        x, y = pos
        tile = layout[y][x]
        match (tile, direction):
            case ('|', Dir.RIGHT) | ('|', Dir.LEFT):
                explore(layout, (x, y-1), Dir.UP, visited)
                direction = Dir.DOWN
            case ('-', Dir.UP) | ('-', Dir.DOWN):
                explore(layout, (x-1, y), Dir.LEFT, visited)
                direction = Dir.RIGHT

            case ('\\', Dir.UP):
                direction = Dir.LEFT
            case ('\\', Dir.RIGHT):
                direction = Dir.DOWN
            case ('\\', Dir.DOWN):
                direction = Dir.RIGHT
            case ('\\', Dir.LEFT):
                direction = Dir.UP

            case ('/', Dir.UP):
                direction = Dir.RIGHT
            case ('/', Dir.RIGHT):
                direction = Dir.UP
            case ('/', Dir.DOWN):
                direction = Dir.LEFT
            case ('/', Dir.LEFT):
                direction = Dir.DOWN

        match direction:
            case Dir.UP:
                pos = (x, y-1)
            case Dir.RIGHT:
                pos = (x+1, y)
            case Dir.DOWN:
                pos = (x, y+1)
            case Dir.LEFT:
                pos = (x-1, y)

def energize(layout, pos, direction):
    visited = {}
    explore(layout, pos, direction, visited)
    return len(visited)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        layout = [line.strip() for line in input_file ]

    print(energize(layout, (0, 0), Dir.RIGHT))

    width = len(layout[0])
    height = len(layout)

    downward = [energize(layout, (x, 0), Dir.DOWN) for x in range(width)]
    upward = [energize(layout, (x, height-1), Dir.UP) for x in range(width)]
    rightward = [energize(layout, (0, y), Dir.RIGHT) for y in range(height)]
    leftward = [energize(layout, (width-1, y), Dir.RIGHT) for y in range(height)]

    print(max(downward+upward+rightward+leftward))

if __name__ == "__main__":
    main()
