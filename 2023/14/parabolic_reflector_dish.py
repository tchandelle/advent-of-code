#!/usr/bin/python3

import functools
import operator
import re

@functools.cache
def tilt_col(col, left = True):
    col = ''.join(col)

    res = ''
    for m in re.finditer(r'([O.]*)(#|$)', col):
        if left:
            res += m[1].count('O') * 'O'
            res += m[1].count('.') * '.'
        else:
            res += m[1].count('.') * '.'
            res += m[1].count('O') * 'O'
        res += m[2]

    return res

def tilt(dish):
    tilted = [ tilt_col(col) for col in zip(*dish)]
    return [*zip(*tilted)]

def cycle(dish):
    dish = [ tilt_col(col, True) for col in zip(*dish)]
    dish = [ tilt_col(col, True) for col in zip(*dish)]
    dish = [ tilt_col(col, False) for col in zip(*dish)]
    dish = [ tilt_col(col, False) for col in zip(*dish)]

    return tuple(dish)

def get_load(dish):
    count = len(dish)
    return [ line.count('O') * (count - idx) for idx, line in enumerate(dish) ]

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        dish = [line.strip() for line in input_file]

    # Part 1
    print(functools.reduce(operator.add, get_load(tilt(dish)), 0))

    # Part 2 - not iterating 1B times. Detect start and length of loop
    visited_dish = []
    dish = tuple(dish)
    while dish not in visited_dish:
        visited_dish.append(dish)
        dish = cycle(dish)

    start_of_loop = visited_dish.index(dish)
    length_of_loop = len(visited_dish) - start_of_loop
    n_in_loop = (1_000_000_000 - start_of_loop) % length_of_loop

    dish = visited_dish[start_of_loop + n_in_loop]
    print(functools.reduce(operator.add, get_load(dish), 0))

if __name__ == "__main__":
    main()
