#!/usr/bin/python3

import functools
import operator
import re

def hash(s):
    val = 0
    for c in s:
        val += ord(c)
        val *= 17
        val %= 256
    return val

assert hash('HASH') == 52

def follow_step(step, boxes):
    m = re.match(r'(.*)(?:-|=(\d+))', step)
    label = m[1]
    box = boxes[hash(m[1])]
    if m[2] is None:
        if label in box:
            box.pop(label)
    else:
        box[label] = int(m[2])

def get_focusing_power(idx, box):
    power = 0
    for slot, focal in enumerate(box.values()):
        power += (idx+1) * (slot+1) * focal
    return power

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        steps = input_file.read().split(',')

    hashes = [ hash(step) for step in steps ]
    print(functools.reduce(operator.add, hashes, 0))

    boxes = [{} for _ in range(256)]
    for step in steps:
        follow_step(step, boxes)

    focusing_powers = [get_focusing_power(idx, box) for idx, box in enumerate(boxes) ]
    print(functools.reduce(operator.add, focusing_powers, 0))

if __name__ == "__main__":
    main()
