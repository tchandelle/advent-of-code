#!/usr/bin/python3

import functools
import operator
import re

def get_calibration_value(data):
    digits = re.findall(r'\d', data)
    if len(digits) == 0:
        return 0

    first = digits[0]
    last = digits.pop()

    return int(first + last)

assert get_calibration_value('1abc2') == 12
assert get_calibration_value('pqr3stu8vwx') == 38
assert get_calibration_value('a1b2c3d4e5f') == 15
assert get_calibration_value('treb7uchet') == 77
assert get_calibration_value('empty') == 0

with open('./input', 'r', encoding='utf-8') as input_file:
    inputs = input_file.readlines()

calibration_values = map(get_calibration_value, inputs)

print(functools.reduce(operator.add, calibration_values, 0))
