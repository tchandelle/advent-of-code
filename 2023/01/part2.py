#!/usr/bin/python3

import functools
import operator
import re

DIGITS_IN_LETTERS = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

def letters_to_digits(data):
    if data in DIGITS_IN_LETTERS:
        return str(DIGITS_IN_LETTERS.index(data) + 1)

    return data

def get_calibration_value(data):
    digits = re.findall(r'(?=(\d|one|two|three|four|five|six|seven|eight|nine))', data)
    if len(digits) == 0:
        return 0

    first = letters_to_digits(digits[0])
    last = letters_to_digits(digits.pop())

    return int(first + last)

assert get_calibration_value('1abc2') == 12
assert get_calibration_value('pqr3stu8vwx') == 38
assert get_calibration_value('a1b2c3d4e5f') == 15
assert get_calibration_value('treb7uchet') == 77
assert get_calibration_value('empty') == 0
assert get_calibration_value('empty') == 0
assert get_calibration_value('two1nine') == 29
assert get_calibration_value('eightwothree') == 83
assert get_calibration_value('abcone2threexyz') == 13
assert get_calibration_value('xtwone3four') == 24
assert get_calibration_value('4nineeightseven2') == 42
assert get_calibration_value('zoneight234') == 14
assert get_calibration_value('7pqrstsixteen') == 76
assert get_calibration_value('one') == 11
assert get_calibration_value('two') == 22
assert get_calibration_value('three') == 33
assert get_calibration_value('four') == 44
assert get_calibration_value('five') == 55
assert get_calibration_value('six') == 66
assert get_calibration_value('seven') == 77
assert get_calibration_value('eight') == 88
assert get_calibration_value('nine') == 99
assert get_calibration_value('twone') == 21
assert get_calibration_value('91twonelt') == 91

with open('./input', 'r', encoding='utf-8') as input_file:
    inputs = input_file.readlines()

calibration_values = map(get_calibration_value, inputs)

print(functools.reduce(operator.add, calibration_values, 0))
