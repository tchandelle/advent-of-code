#!/usr/bin/python3

import functools
import operator
import re

@functools.cache
def count_arrangements(row, spring):
    if len(spring) == 1:
        count = 0
        i = 0
        j = spring[0]
        damaged_total = row.count('#')
        while j <= len(row):
            unknown = row[i:j].count('?')
            damaged = row[i:j].count('#')
            if damaged == damaged_total and unknown + damaged == spring[0]:
                count += 1

            i += 1
            j += 1

        return count

    count = 0
    i = 0
    j = spring[0]

    while j < len(row) - 1:
        while j < len(row) - 1 and row[j] == '#':
            if row[i] == '#':
                return count
            i += 1
            j += 1

        left = count_arrangements(row[i:j].strip('.'), spring[0:1])
        if left != 0:
            right = count_arrangements(row[j+1:].strip('.'), spring[1:])
            count += left * right

        if row[i] == '#':
            return count
        i += 1
        j += 1

    return count


assert count_arrangements('???', (1,)) == 3
assert count_arrangements('###', (1,)) == 0
assert count_arrangements('?#?', (1,)) == 1
assert count_arrangements('???.###', (1,3)) == 3
assert count_arrangements('???.###', (1,1,3)) == 1
assert count_arrangements('??..??...?##', (1,1,3)) == 4
assert count_arrangements('?#?#?#?#?#?#?#?', (1,3,1,6)) == 1
assert count_arrangements('????.#...#...', (4,1,1)) == 1
assert count_arrangements('????.######..#####.', (1,6,5)) == 4
assert count_arrangements('?###????????', (3,2,1)) == 10

def parse(s):
    row, springs = s.strip().split()
    springs = [int(i) for i in springs.split(',')]
    return row, tuple(springs)

def unfold(parsed):
    row, springs = parsed
    return clean('?'.join([row] * 5)), springs * 5

def clean(before):
    after = re.sub(r'\.+', '.', before.strip('.'))
    return after

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        parsed = [ parse(line) for line in input_file ]

    arrangements = [count_arrangements(row, springs) for row, springs in parsed]
    print(functools.reduce(operator.add, arrangements, 0))

    unfolded = [ unfold(line) for line in parsed ]
    arrangements = [count_arrangements(row, springs) for row, springs in unfolded]
    print(functools.reduce(operator.add, arrangements, 0))

if __name__ == "__main__":
    main()
