#!/usr/bin/python3

import queue

LOW, HIGH = False, True

def push_the_button(modules, states):
    q = queue.Queue()
    q.put(('button', 'broadcaster', LOW))

    high, low = 0, 0
    while not q.empty():
        src, dest, pulse = q.get()
        if pulse == HIGH:
            high += 1
        else:
            low += 1
        # print(f'{src} -{"high" if pulse else "low"}-> {dest}')
        flip_flop = '%'+dest
        conjunction = '&'+dest
        if dest == 'broadcaster':
            for mod in modules[dest]:
                q.put((dest, mod, pulse))
        elif flip_flop in modules:
            if pulse == LOW:
                pulse = not states[flip_flop]
                states[flip_flop] = pulse
                for mod in modules[flip_flop]:
                    q.put((flip_flop, mod, pulse))
        elif conjunction in modules:
            states[conjunction][src] = pulse
            pulse = not all(states[conjunction].values())
            for mod in modules[conjunction]:
                q.put((conjunction, mod, pulse))

    return high, low

def set_states(modules):
    states = {}
    conjunction = [name for name in modules.keys() if name.startswith('&')]
    for name in conjunction:
        states[name] = {}
    for name, targets in modules.items():
        if name.startswith('%'):
            states[name] = False
        for target in targets:
            if '&'+target in conjunction:
                states['&'+target][name] = False

    return states


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        modules = { line.split('->')[0].strip(): line.split('->')[1].strip().split(', ') for line in input_file }

    states = set_states(modules)
    high, low = 0, 0
    for _ in range(1000):
        h, l = push_the_button(modules, states)
        high += h
        low += l
    print(high * low)

if __name__ == "__main__":
    main()
