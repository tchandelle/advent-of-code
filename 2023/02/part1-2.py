#!/usr/bin/python3

from enum import Enum
import functools
import operator
import re

class Color(Enum):
    RED, GREEN, BLUE = range(0, 3)

def parse_sub_game(data):
    cubes = [0, 0, 0]
    for cube in data.split(','):
        m = re.match(r'\s*(\d+) (\D+)\s*', cube)
        color_str = m[2].upper()
        cubes[Color[color_str].value] = int(m[1])

    return cubes

def is_sub_game_possible(sub_game, in_bag):
    return (sub_game[0] <= in_bag[0] and
            sub_game[1] <= in_bag[1] and
            sub_game[2] <= in_bag[2])

def parse_games(data):
    games = data.split(';')
    return list(map(parse_sub_game, games))

class Game:
    def __init__(self, data):
        m = re.match(r'^Game (\d+): (.*)$', data)
        self.id = int(m[1])
        self.games = parse_games(m[2])

    def is_game_possible(self, in_bag):
        return all(map(lambda sub_game : is_sub_game_possible(sub_game, in_bag), self.games))

    def get_min(self):
        reds = map(lambda g : g[0], self.games)
        greens = map(lambda g : g[1], self.games)
        blues = map(lambda g : g[2], self.games)

        return [max(reds), max(greens), max(blues)]

    def get_power(self):
        return functools.reduce(operator.mul, self.get_min(), 1)

def main():
    in_bag = [12, 13, 14]

    games = [
        Game("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
        Game("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"),
        Game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"),
        Game("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"),
        Game("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green")
    ]

    assert games[0].is_game_possible(in_bag) is True
    assert games[0].get_min() == [4, 2, 6]
    assert games[0].get_power() == 48
    assert games[1].is_game_possible(in_bag) is True
    assert games[1].get_min() == [1, 3, 4]
    assert games[1].get_power() == 12
    assert games[2].is_game_possible(in_bag) is False
    assert games[2].get_min() == [20, 13, 6]
    assert games[2].get_power() == 1560
    assert games[3].is_game_possible(in_bag) is False
    assert games[3].get_min() == [14, 3, 15]
    assert games[3].get_power() == 630
    assert games[4].is_game_possible(in_bag) is True
    assert games[4].get_min() == [6, 3, 2]
    assert games[4].get_power() == 36

    with open('./input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    games = list(map(Game, inputs))
    possible_games = filter(lambda g : g.is_game_possible(in_bag), games)
    possible_games_id = map(lambda g : g.id, possible_games)

    print(functools.reduce(operator.add, possible_games_id, 0))

    print(functools.reduce(operator.add, map(Game.get_power, games), 0))

if __name__ == "__main__":
    main()
