#!/usr/bin/python3

import re

def parse_1(line):
    m = re.match(r'^([UDLR]) (\d+)', line)
    return m[1], int(m[2])

DIRECTIONS = {
    '0': 'R',
    '1': 'D',
    '2': 'L',
    '3': 'U',
}

def parse_2(line):
    m = re.match(r'.*\(#([0-9a-f]{6})\)', line)

    return DIRECTIONS[m[1][5]], int(m[1][0:5], 16)

VECTORS = {
    'U': -1j,
    'D': +1j,
    'L': -1,
    'R': +1,
}

def dig(dig_plan):
    layout = []

    pos = 0+0j
    trench_length = 0
    layout.append(pos)

    for direction, length in dig_plan:
        pos += VECTORS[direction] * length
        trench_length += length
        layout.append(pos)

    return layout, trench_length

def dig_interior(layout, trench_length):
    # shoelace formula
    sum1 = 0
    sum2 = 0
    for a, b in zip(layout, layout[1:]):
        sum1 += a.real * b.imag
        sum2 += a.imag * b.real

    inside = (sum1-sum2) / 2
    trench = (trench_length-4) / 2 + 3

    return int(inside + trench)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        dig_plan = [ parse_1(line) for line in input_file ]
    layout, trench_length = dig(dig_plan)
    print(dig_interior(layout, trench_length))

    with open('input', 'r', encoding='utf-8') as input_file:
        dig_plan = [ parse_2(line) for line in input_file ]
    layout, trench_length = dig(dig_plan)
    print(dig_interior(layout, trench_length))

if __name__ == "__main__":
    main()
