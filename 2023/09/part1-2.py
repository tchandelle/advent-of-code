#!/usr/bin/python3

import functools
import operator

def diff(numbers):
    return list(map(lambda ab: ab[1] - ab[0], zip(numbers, numbers[1:])))

def all_zeroes(numbers):
    return all(map(lambda n : n == 0, numbers))

def build_history(first):
    history = [first]
    last = first
    while not all_zeroes(last):
        last = diff(last)
        history.append(last)

    return history

def extrapolate(numbers):
    history = build_history(numbers)

    history.reverse()

    for prev, current in zip(history, history[1:]):
        current.insert(0, current[0] - prev[0])
        current.append(current.pop() + prev.pop())

    last = history.pop()
    return last[0], last.pop()

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        serialized = map(lambda line : list(map(int, line.split())), input_file)
        values = list(map(extrapolate, serialized))

        print(functools.reduce(operator.add, map(lambda t : t[1], values), 0))
        print(functools.reduce(operator.add, map(lambda t : t[0], values), 0))

if __name__ == "__main__":
    main()
