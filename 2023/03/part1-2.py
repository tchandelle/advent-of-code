#!/usr/bin/python3

import functools
import operator
import re

def find_symbol_position(lines, left, right, y):
    match = re.match(r'.*([^\d\.]).*', lines[y][left:right])
    return (left + match.start(1), y) if match is not None else None

def check_number(lines, y, match):
    number = int(match[1])

    left = max(match.start(1) - 1, 0)
    right = match.end(1) + 1

    for y in range(max(0, y - 1), min(len(lines), y + 2)):
        pos = find_symbol_position(lines, left, right, y)
        if pos is not None:
            return number, pos

def find_number(lines, y):
    return filter(
        lambda num : num is not None,
        map(
            lambda match : check_number(lines, y, match),
            re.finditer(r'(\d+)', lines[y])
        )
    )

def main():
    with open('./input', 'r', encoding='utf-8') as input_file:
        lines = list(map(str.strip, input_file.readlines()))

    results = []

    for y in range(len(lines)):
        results.extend(find_number(lines, y))

    print(functools.reduce(operator.add, map(lambda tuple : tuple[0], results), 0))

    symbols = {}
    for num, pos in results:
        if pos in symbols:
            symbols[pos].append(num)
        else:
            symbols[pos] = [ num ]

    sum = 0
    for pos, nums in symbols.items():
        if len(nums) == 2:
            sum += nums[0] * nums[1]

    print(sum)

if __name__ == "__main__":
    main()
