#!/usr/bin/python3

import functools
import operator
import re

class Card:
    def __init__(self, data):
        m = re.match(r'^Card\s+(\d+): ([\d\s]*) \| ([\d\s]*)$', data.strip())
        self.id = int(m[1])
        self.winning = list(map(int, m[2].split()))
        self.my_numbers = list(map(int, m[3].split()))
        self.copy = 1

    def get_winning_numbers(self):
        return list(filter(lambda n : n in self.winning, self.my_numbers))

    def get_number_of_winning(self):
        return len(self.get_winning_numbers())

    def get_points(self):
        winning = self.get_number_of_winning()
        return 0 if winning == 0 else 2**(winning - 1)

def main():
    with open('./input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    cards = map(Card, inputs)

    points = map(Card.get_points, cards)
    print(functools.reduce(operator.add, points, 0))


if __name__ == "__main__":
    main()
