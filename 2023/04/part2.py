#!/usr/bin/python3

import functools
import operator
from part1 import Card

def main():
    with open('./input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    cards = list(map(Card, inputs))

    copies = 0

    for idx, card in enumerate(cards):
        copies += card.copy
        for x in range(card.get_number_of_winning()):
            if len(cards) > idx + x + 1:
                cards[idx + x + 1].copy += card.copy

    print(copies)


if __name__ == "__main__":
    main()
