#!/usr/bin/python3

import queue

DIRECTIONS = [-1j, +1j, -1, +1]

def walk(rocks, start, steps):
    # BFS
    dist = {start: steps}
    q = queue.Queue()
    q.put(start)

    while not q.empty():
        pos = q.get()
        for d in DIRECTIONS:
            next_pos = pos + d
            if next_pos in rocks or next_pos in dist:
                continue
            dist[next_pos] = dist[pos] - 1
            if dist[next_pos] >= 0:
                q.put(next_pos)

    return dist

def print_map(reached, rocks):
    for y in range(130):
        for x in range(130):
            if complex(x, y) in reached:
                print('O', end='')
            elif complex(x, y) in rocks:
                print('#', end='')
            else:
                print('.', end='')
        print('')

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        rocks = []
        for y, line in enumerate(input_file):
            for x, c in enumerate(line):
                if c == '#':
                    rocks.append(complex(x, y))
                elif c == 'S':
                    start = complex(x, y)

    steps = 64
    reached = walk(rocks, start, steps)

    even_odd = [0, 0]
    for v in reached.values():
        even_odd[v%2] += 1

    print(even_odd[steps%2])

if __name__ == "__main__":
    main()
