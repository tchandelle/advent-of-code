#!/usr/bin/python3

import functools
import operator

def get_horizontal_line(plan, with_smudge = False):
    candidates = list(range(1, len(plan)))
    if with_smudge:
        candidates *= 2
    height = len(plan)

    for x in range(len(plan[0])):
        for y in set(candidates):
            compare_height = min(y, height - y)
            top = [line[x] for line in plan[y-compare_height:y]]
            bottom = [line[x] for line in plan[y:y+compare_height]]
            bottom.reverse()
            if top != bottom:
                candidates.remove(y)

    # smudge correction: correct candidate should be alone, not duplicated
    while candidates:
        y = candidates.pop()
        if y in candidates:
            candidates.remove(y)
        else:
            return y
    return 0

def get_vertical_line(plan, with_smudge = False):
    candidates = list(range(1, len(plan[0])))
    if with_smudge:
        candidates *= 2
    width = len(plan[0])

    for line in plan:
        for x in set(candidates):
            compare_width = min(x, width - x)
            left = line[x-compare_width:x]
            right = line[x:x+compare_width]
            if left != right[::-1]:
                candidates.remove(x)

    while candidates:
        x = candidates.pop()
        if x in candidates:
            candidates.remove(x)
        else:
            return x
    return 0

def find_line_of_reflection(plan):
    return 100 * get_horizontal_line(plan) + get_vertical_line(plan)

def find_line_of_reflection_with_smudge(plan):
    return 100 * get_horizontal_line(plan, True) + get_vertical_line(plan, True)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        plans = [[]]
        for line in input_file:
            if line.strip() == '':
                plans.append([])
            else:
                plans[-1].append(line.strip())

    print(functools.reduce(operator.add, map(find_line_of_reflection, plans), 0))
    print(functools.reduce(operator.add, map(find_line_of_reflection_with_smudge, plans), 0))


if __name__ == "__main__":
    main()
