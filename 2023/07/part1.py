#!/usr/bin/python3

import functools
import operator

CARDS = '23456789TJQKA'
HIGH, ONE_PAIR, TWO_PAIR, THREE, FULL_HOUSE, FOUR, FIVE = range(7)

def type_of_hand(hand):
    cards = {}
    for c in hand:
        if c in cards:
            cards[c] += 1
        else:
            cards[c] = 1

    fives = len(list(filter(lambda n : n == 5, cards.values())))
    fours = len(list(filter(lambda n : n == 4, cards.values())))
    threes = len(list(filter(lambda n : n == 3, cards.values())))
    twos = len(list(filter(lambda n : n == 2, cards.values())))

    if fives == 1:
        return FIVE
    if fours == 1:
        return FOUR
    if threes == 1 and twos == 1:
        return FULL_HOUSE
    if threes == 1:
        return THREE
    if twos == 2:
        return TWO_PAIR
    if twos == 1:
        return ONE_PAIR

    return HIGH

def score(hand):
    return ((type_of_hand(hand) << 20) +
        (CARDS.index(hand[0]) << 16) +
        (CARDS.index(hand[1]) << 12) +
        (CARDS.index(hand[2]) << 8) +
        (CARDS.index(hand[3]) << 4) +
        (CARDS.index(hand[4]) << 0))

def deserialize(data):
    hand, bet = data.split()

    return hand, score(hand), int(bet)

def winning(t):
    rank, hand = t
    return rank * hand[2]

def main():
    assert type_of_hand('AAAAA') == FIVE
    assert type_of_hand('AA8AA') == FOUR
    assert type_of_hand('23332') == FULL_HOUSE
    assert type_of_hand('TTT98') == THREE
    assert type_of_hand('23432') == TWO_PAIR
    assert type_of_hand('A23A4') == ONE_PAIR
    assert type_of_hand('23456') == HIGH

    with open('input', 'r', encoding='utf-8') as input_file:
        hands = map(deserialize, input_file.readlines())

    hands = sorted(hands, key = lambda h : h[1])

    print(functools.reduce(operator.add, map(winning, enumerate(hands, start=1)), 0))


if __name__ == "__main__":
    main()
