#!/usr/bin/python3

import functools
import operator
import re

def parse_workflow(line):
    start_rules = line.index('{')
    rules = [ (m[1], m[2], m[3], m[4]) for m in re.finditer(r'(?:([xmas])?([<>])?(\d+)?:)?(\w+)', line[start_rules:]) ]
    return line[:start_rules], rules

def parse_rating(line):
    return { m[1]: int(m[2]) for m in re.finditer(r'([xmas])=(\d+)', line)}

def eval_workflow(rating, rules):
    for cat, oper, operand, target in rules:
        if cat is None:
            return target
        if oper == '<' and rating[cat] < int(operand):
            return target
        if oper == '>' and rating[cat] > int(operand):
            return target

def follow_workflow(rating, workflows):
    w = workflows['in']

    while True:
        res = eval_workflow(rating, w)
        match res:
            case 'A':
                return True
            case 'R':
                return False
        w = workflows[res]

def get_ratings(workflows, name, rating):
    rating = rating.copy()
    match name:
        case 'A':
            return [rating]
        case 'R':
            return []

    ratings = []
    for cat, oper, operand, target in workflows[name]:
        match oper:
            case '<':
                rating_copy = rating.copy()
                rating_copy[cat] = range(rating_copy[cat].start, int(operand))
                ratings += get_ratings(workflows, target, rating_copy)

                rating[cat] = range(int(operand), rating[cat].stop)
            case '>':
                rating_copy = rating.copy()
                rating_copy[cat] = range(int(operand)+1, rating_copy[cat].stop)
                ratings += get_ratings(workflows, target, rating_copy)

                rating[cat] = range(rating[cat].start, int(operand)+1)
            case _:
                return ratings + get_ratings(workflows, target, rating)

    return ratings

def count_ratings(workflows):
    rating = { key: range(1, 4001) for key in 'xmas' }
    ratings = get_ratings(workflows, 'in', rating)

    return functools.reduce(
        operator.add,
        [
            functools.reduce(operator.mul, [len(rang) for rang in rat.values()], 1)
            for rat in ratings
        ]
    )

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        workflows = {}
        ratings = []
        feed_workflows = True
        for line in input_file:
            if line.strip() == '':
                feed_workflows = False
            elif feed_workflows:
                name, rules = parse_workflow(line)
                workflows[name] = rules
            else:
                ratings.append(parse_rating(line))

    accepted = []
    for rating in ratings:
        if follow_workflow(rating, workflows):
            accepted.extend(rating.values())

    print(functools.reduce(operator.add, accepted, 0))

    print(count_ratings(workflows))

if __name__ == "__main__":
    main()
