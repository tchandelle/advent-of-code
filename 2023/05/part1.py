#!/usr/bin/python3

import functools
import re

def try_converting(source, m):
    dest_range_start, src_range_start, range_length = m
    diff = source - src_range_start
    if diff < 0 or diff >= range_length:
        return None
    return dest_range_start + diff

def convert_with_map(source, maps):
    for m in maps:
        dest = try_converting(source, m)
        if dest is not None:
            return dest
    return source

def convert_with_maps(source, maps):
    return functools.reduce(convert_with_map, maps, source)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    seeds = []
    maps = [[], [], [], [], [], [], []]

    for line in inputs:
        match line.strip():
            case 'seed-to-soil map:':
                current = maps[0]
            case 'soil-to-fertilizer map:':
                current = maps[1]
            case 'fertilizer-to-water map:':
                current = maps[2]
            case 'water-to-light map:':
                current = maps[3]
            case 'light-to-temperature map:':
                current = maps[4]
            case 'temperature-to-humidity map:':
                current = maps[5]
            case 'humidity-to-location map:':
                current = maps[6]
            case '':
                pass
            case _:
                m = re.match(r'^(seeds: )?([\d\s]*)$', line)
                numbers = list(map(int, m[2].split()))
                if m[1] is None:
                    current.append(numbers)
                else:
                    seeds = numbers

    seed_to_location = map(lambda seed : convert_with_maps(seed, maps), seeds)
    print(min(seed_to_location))

if __name__ == "__main__":
    main()
