#!/usr/bin/python3

import functools
import re
import sys

def to_ranges(seeds):
    return list(map(
        lambda x : range(seeds[x], seeds[x] + seeds[x+1]),
        range(0, len(seeds), 2)
    ))

def is_in_seeds(seed, seeds):
    for seed_range in seeds:
        if seed in seed_range:
            return True
    return False

def try_converting(dest, m):
    dest_range_start, src_range_start, range_length = m
    diff = dest - dest_range_start
    if diff < 0 or diff >= range_length:
        return None
    return src_range_start + diff

def convert_with_map(dest, maps):
    for m in maps:
        source = try_converting(dest, m)
        if source is not None:
            return source
    return dest

def convert_with_maps(dest, maps):
    return functools.reduce(convert_with_map, maps, dest)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    seeds = []
    maps = [[], [], [], [], [], [], []]

    for line in inputs:
        match line.strip():
            case 'seed-to-soil map:':
                current = maps[6]
            case 'soil-to-fertilizer map:':
                current = maps[5]
            case 'fertilizer-to-water map:':
                current = maps[4]
            case 'water-to-light map:':
                current = maps[3]
            case 'light-to-temperature map:':
                current = maps[2]
            case 'temperature-to-humidity map:':
                current = maps[1]
            case 'humidity-to-location map:':
                current = maps[0]
            case '':
                pass
            case _:
                m = re.match(r'^(seeds: )?([\d\s]*)$', line)
                numbers = list(map(int, m[2].split()))
                if m[1] is None:
                    current.append(numbers)
                else:
                    seeds = to_ranges(numbers)

    # First, get a rough and fast estimation:
    # for dest in range(0, 2**32, 1000):
    for dest in range(60000000, 2**32):
        source = convert_with_maps(dest, maps)
        if is_in_seeds(source, seeds):
            print(f"location = {dest}")
            print(f"seed = {source}")
            sys.exit(0)

if __name__ == "__main__":
    main()
