#!/usr/bin/python3

OUTSIDE, INSIDE, UP, RIGHT, DOWN, LEFT = range(0, 6)

def get_tile(plan, pos):
    if pos[1] < 0 or pos[1] >= len(plan) or pos[0] < 0 or pos[0] >= len(plan[0]):
        return '.'

    return plan[pos[1]][pos[0]]

def find_start(plan):
    for y, line in enumerate(plan):
        if 'S' in line:
            return (line.index('S'), y)

def next_pos(pos, direction):
    if direction == UP:
        return (pos[0], pos[1] - 1)
    if direction == RIGHT:
        return (pos[0] + 1, pos[1])
    if direction == DOWN:
        return (pos[0], pos[1] + 1)
    if direction == LEFT:
        return (pos[0] - 1, pos[1])

def find_start_dir(plan, pos):
    directions = []
    if get_tile(plan, next_pos(pos, UP)) in '|7F':
        directions.append(UP)
    if get_tile(plan, next_pos(pos, LEFT)) in '-LF':
        directions.append(LEFT)
    if get_tile(plan, next_pos(pos, RIGHT)) in '-J7':
        directions.append(RIGHT)
    if get_tile(plan, next_pos(pos, DOWN)) in '|JL':
        directions.append(DOWN)
    return directions

def real_start_tile(directions):
    if directions == [UP, LEFT]:
        return 'J'
    if directions == [UP, RIGHT]:
        return 'L'
    if directions == [UP, DOWN]:
        return '|'
    if directions == [LEFT, RIGHT]:
        return '-'
    if directions == [LEFT, DOWN]:
        return '7'
    if directions == [RIGHT, DOWN]:
        return 'F'

def travel(plan):
    position = find_start(plan)
    tile = ''
    directions = find_start_dir(plan, position)
    start_tile = real_start_tile(directions)
    direction = directions[0]

    edge = []
    while tile != 'S':
        edge.append(position)
        match tile:
            case '|':
                direction = UP if direction == UP else DOWN
            case '-':
                direction = RIGHT if direction == RIGHT else LEFT
            case 'L':
                direction = UP if direction == LEFT else RIGHT
            case 'J':
                direction = UP if direction == RIGHT else LEFT
            case '7':
                direction = DOWN if direction == RIGHT else LEFT
            case 'F':
                direction = DOWN if direction == LEFT else RIGHT

        position = next_pos(position, direction)
        tile = get_tile(plan, position)

    plan[position[1]][position[0]] = start_tile
    return edge

def get_enclosed(plan, edge):
    enclosed = 0
    for y, line in enumerate(plan):
        inside = OUTSIDE
        for x, tile in enumerate(line):
            if (x, y) in edge:
                match tile:
                    case '|':
                        inside = not inside
                    case 'J':
                        inside = INSIDE if inside == DOWN else OUTSIDE
                    case '7':
                        inside = INSIDE if inside == UP else OUTSIDE
                    case 'L':
                        inside = DOWN if inside == INSIDE else UP
                    case 'F':
                        inside = UP if inside == INSIDE else DOWN
            elif inside:
                enclosed += 1

    return enclosed

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        plan = [ list(line.strip()) for line in input_file ]
        edge = travel(plan)

        print(len(edge) // 2)

        print(get_enclosed(plan, edge))

if __name__ == "__main__":
    main()
