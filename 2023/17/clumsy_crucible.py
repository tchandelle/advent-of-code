#!/usr/bin/python3

import math
from enum import Enum

class Dir(int, Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

def neighbors(v, minimum):
    x, y, z, d = v
    if z < minimum:
        match d:
            case Dir.UP:
                return ( (x, y-1, z+1, Dir.UP), )
            case Dir.RIGHT:
                return ( (x+1, y, z+1, Dir.RIGHT), )
            case Dir.DOWN:
                return ( (x, y+1, z+1, Dir.DOWN), )
            case Dir.LEFT:
                return ( (x-1, y, z+1, Dir.LEFT), )
    match d:
        case Dir.UP:
            return (
                (x, y-1, z+1, Dir.UP),
                (x-1, y, 0, Dir.LEFT),
                (x+1, y, 0, Dir.RIGHT),
            )
        case Dir.RIGHT:
            return (
                (x+1, y, z+1, Dir.RIGHT),
                (x, y-1, 0, Dir.UP),
                (x, y+1, 0, Dir.DOWN),
            )
        case Dir.DOWN:
            return (
                (x, y+1, z+1, Dir.DOWN),
                (x-1, y, 0, Dir.LEFT),
                (x+1, y, 0, Dir.RIGHT),
            )
        case Dir.LEFT:
            return (
                (x-1, y, z+1, Dir.LEFT),
                (x, y-1, 0, Dir.UP),
                (x, y+1, 0, Dir.DOWN),
            )

def dijkstra(layout, sources, destinations, minimum):
    dist = { k: math.inf for k in layout.keys() }
    q = set(layout.keys())
    dist_q = [set() for _ in range(10000)]

    for source in sources:
        dist[source] = 0
        dist_q[0].add(source)
    cur_min = 0

    while q:
        while not dist_q[cur_min]:
            cur_min += 1

        u = dist_q[cur_min].pop()
        q.remove(u)

        if u in destinations:
            return dist[u]

        for v in neighbors(u, minimum):
            if v not in q:
                continue
            alt = dist[u] + layout[v]
            if alt < dist[v]:
                dist[v] = alt
                dist_q[alt].add(v)

def get_layout(maximum):
    with open('input', 'r', encoding='utf-8') as input_file:
        return {
            (x, y, z, d): int(w)
            for y, line in enumerate(input_file)
            for x, w in enumerate(line.strip())
            for z in range(maximum)
            for d in Dir
        }

def main():
    layout1 = get_layout(3)
    layout2 = get_layout(10)

    last = list(layout1)[-1]

    sources = ((0, 0, 0, Dir.RIGHT), (0, 0, 0, Dir.DOWN))
    destinations1 = [
        (last[0], last[1], z, d)
        for z in range(3)
        for d in [Dir.RIGHT, Dir.DOWN]
    ]
    shortest = dijkstra(layout1, sources, destinations1, -1)
    print(shortest)

    destinations2 = [
        (last[0], last[1], z, d)
        for z in range(3, 10)
        for d in [Dir.RIGHT, Dir.DOWN]
    ]
    shortest = dijkstra(layout2, sources, destinations2, 3 )
    print(shortest)


if __name__ == "__main__":
    main()
