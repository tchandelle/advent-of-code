#!/usr/bin/python3

import math
import re

def count_steps(graph, directions, current = 'AAA'):
    steps = 0
    while True:
        for d in directions:
            steps += 1
            match d:
                case 'L':
                    current = graph[current][0]
                case 'R':
                    current = graph[current][1]

            if current[2] == 'Z':
                return steps

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    starts = []
    graph = {}

    for line in inputs:
        m_dir = re.match(r'^([LR]+)$', line)
        if m_dir is not None:
            directions = m_dir[1]

        m_graph = re.match(r'^([A-Z1-9]{3}) = \(([A-Z1-9]{3}), ([A-Z1-9]{3})\)$', line)
        if m_graph is not None:
            graph[m_graph[1]] = [m_graph[2], m_graph[3]]
            if m_graph[1][2] == 'A':
                starts.append(m_graph[1])

    steps = map(lambda start : count_steps(graph, directions, start), starts)
    print(math.lcm(*steps))

if __name__ == "__main__":
    main()
