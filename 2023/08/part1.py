#!/usr/bin/python3

import re

def count_steps(graph, directions, current = 'AAA'):
    steps = 0
    while True:
        for d in directions:
            steps += 1
            match d:
                case 'L':
                    current = graph[current][0]
                case 'R':
                    current = graph[current][1]

            if current == 'ZZZ':
                return steps

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        inputs = input_file.readlines()

    graph = {}

    for line in inputs:
        m_dir = re.match(r'^([LR]+)$', line)
        if m_dir is not None:
            directions = m_dir[1]

        m_graph = re.match(r'^([A-Z]{3}) = \(([A-Z]{3}), ([A-Z]{3})\)$', line)
        if m_graph is not None:
            graph[m_graph[1]] = [m_graph[2], m_graph[3]]

    steps = count_steps(graph, directions)
    print(steps)

if __name__ == "__main__":
    main()
