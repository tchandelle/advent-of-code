#!/usr/bin/python3

import functools
import operator

def get_galaxies(plan):
    galaxies = []
    for y, line in enumerate(plan):
        for x, char in enumerate(line):
            if char == '#':
                galaxies.append((x, y))
    return galaxies

def get_expandable_rows(plan):
    empty = set()
    for y, line in enumerate(plan):
        if not '#' in line:
            empty.add(y)
    return empty

def get_expandable_cols(plan):
    empty = set()
    for x in range(len(plan[0])):
        col = [line[x] for line in plan]
        if not '#' in col:
            empty.add(x)
    return empty

def pair_galaxies(galaxies):
    pairs = []
    if len(galaxies) == 0:
        return pairs

    a, other = galaxies[0], galaxies[1:]
    for b in other:
        pairs.append((a, b))

    pairs.extend(pair_galaxies(other))
    return pairs

def distance(pair, expandable_rows, expandable_cols, expansion):
    a, b = pair
    left, right = min(a[0], b[0]), max(a[0], b[0])
    top, bottom = min(a[1], b[1]), max(a[1], b[1])

    steps = 0

    for x in range(left, right):
        steps += expansion if x in expandable_cols else 1

    for y in range(top, bottom):
        steps += expansion if y in expandable_rows else 1

    return steps


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        plan = [ list(line.strip()) for line in input_file ]

    galaxies = pair_galaxies(get_galaxies(plan))
    expandable_rows = get_expandable_rows(plan)
    expandable_cols = get_expandable_cols(plan)

    distances = [distance(pair, expandable_rows, expandable_cols, 2) for pair in galaxies]
    print(functools.reduce(operator.add, distances, 0))

    distances = [distance(pair, expandable_rows, expandable_cols, 1000000) for pair in galaxies]
    print(functools.reduce(operator.add, distances, 0))

if __name__ == "__main__":
    main()
