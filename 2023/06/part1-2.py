#!/usr/bin/python3

import functools
import operator

def distance(hold, time):
    return hold * (time - hold)

def race(time, min_dist):
    count = 0
    for hold in range(time):
        if distance(hold, time) > min_dist:
            count += 1

    return count


def main():
    races = [
        race(40, 215),
        race(70, 1051),
        race(98, 2147),
        race(79, 1005),
    ]

    print(functools.reduce(operator.mul, races, 1))

    print(race(40709879, 215105121471005))


if __name__ == "__main__":
    main()
