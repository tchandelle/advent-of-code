#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  vector<string> res;
  string s;
  while (getline(input, s))
    res.push_back(s);

  return res;
}

void increase_power(
    vector<string> &octopi,
    set<pair<int, int>> &flashes,
    int x, int y)
{
  if (y < 0 || y > octopi.size() - 1 || x < 0 || x > octopi[y].size() - 1)
    return;
  if (octopi[y][x] > '9')
    return;
  if (++octopi[y][x] <= '9')
    return;

  flashes.insert({x, y});

  increase_power(octopi, flashes, x - 1, y - 1);
  increase_power(octopi, flashes, x, y - 1);
  increase_power(octopi, flashes, x + 1, y - 1);

  increase_power(octopi, flashes, x - 1, y);
  increase_power(octopi, flashes, x + 1, y);

  increase_power(octopi, flashes, x - 1, y + 1);
  increase_power(octopi, flashes, x, y + 1);
  increase_power(octopi, flashes, x + 1, y + 1);
}

int single_step(vector<string> &octopi)
{
  set<pair<int, int>> flashes;
  for (int y = 0; y < octopi.size(); ++y)
  {
    string &line = octopi[y];
    for (int x = 0; x < line.size(); ++x)
    {
      increase_power(octopi, flashes, x, y);
    }
  }
  for (auto xy : flashes)
  {
    octopi[xy.second][xy.first] = '0';
  }
  return flashes.size();
}

int main()
{
  auto octopi = get_data();

  int max_flashes = octopi.size() * octopi[0].size();

  int flashes_single_step = 0;
  int flashes = 0;

  int steps = 1;
  while ((flashes_single_step = single_step(octopi)) != max_flashes)
  {
    if (steps <= 100)
      flashes += flashes_single_step;
    steps++;
  }

  cout << flashes << endl;
  cout << steps << endl;

  return 0;
}