#include <deque>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  deque<unsigned long long> res(9);

  unsigned long long n;
  char c;
  while (input >> n >> c)
  {
    ++res[n];
  }
  ++res[n];

  input.close();

  return res;
}

unsigned long long count_fishes(deque<unsigned long long> fishes, int generations)
{
  for (int i = generations; i > 0; --i)
  {
    unsigned long long zero = fishes.front();
    fishes.pop_front();
    fishes.push_back(0);

    fishes[6] += zero;
    fishes[8] += zero;
  }

  unsigned long long count = 0;
  for (unsigned long long fish : fishes)
    count += fish;

  return count;
}

int main()
{
  // double-ended queue, where
  // - index is the internal timer
  // - value is the number of lanternfishes.
  auto fishes = get_data();

  cout << count_fishes(fishes, 80) << endl;
  cout << count_fishes(fishes, 256) << endl;

  return 0;
}
