#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

vector<int> get_data()
{
  ifstream input(::input);

  vector<int> depths;

  int n;
  while (input >> n)
  {
    depths.push_back(n);
  }
  input.close();

  return depths;
}

vector<int> three_measurement(const vector<int> &depths)
{
  vector<int> ret;
  ret.reserve(depths.size() - 2);

  for (auto it = depths.begin(); it < depths.end() - 2; ++it)
  {
    ret.push_back(*it + *(it + 1) + *(it + 2));
  }

  return ret;
}

int count_increase(const vector<int> &depths)
{
  int increases = 0;
  int prev = depths.front();
  for (int d : vector<int>{depths.begin() + 1, depths.end()})
  {
    if (prev < d)
    {
      increases++;
    }
    prev = d;
  }

  return increases;
}

int main()
{
  vector<int> depths = get_data();
  cout << count_increase(depths) << endl;

  vector<int> depths3 = three_measurement(depths);
  cout << count_increase(depths3) << endl;

  return 0;
}
