#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  vector<string> binaries;

  string n;
  while (input >> n)
  {
    binaries.push_back(n);
  }
  input.close();

  return binaries;
}

pair<int, int> get_rates(const vector<string> &binaries)
{
  vector<int> count(binaries[0].size());

  for (string bin : binaries)
  {
    for (int i = 0; i < count.size(); ++i)
    {
      if (bin[i] == '1')
        ++count[i];
    }
  }

  int gamma = 0, epsilon = 0;
  for (int i = 0; i < count.size(); ++i)
  {
    if (count[i] > binaries.size() / 2)
      gamma |= 1 << (count.size() - i - 1);
    else
      epsilon |= 1 << (count.size() - i - 1);
  }
  return {gamma, epsilon};
}

char get_majority(
    vector<string>::iterator left,
    vector<string>::iterator right,
    int i,
    char def)
{
  auto diff = right - left;
  auto mid = left + diff / 2;

  if (diff % 2 == 1)
    return (*mid)[i];

  if ((*mid)[i] == (*(mid - 1))[i])
    return (*mid)[i];

  return def;
}

string get_rate2(
    vector<string>::iterator begin,
    vector<string>::iterator end,
    char most_common,
    int i = 0)
{
  if (begin + 1 == end)
    return *begin;

  auto left = begin;
  auto right = end;

  char majority = get_majority(left, right, i, '1');

  while ((left + 1) != right)
  {
    auto mid = left + (right - left) / 2;

    if ((*mid)[i] == '1')
      right = mid;
    else
      left = mid;
  }

  if (majority == most_common)
    return get_rate2(right, end, most_common, i + 1);
  else
    return get_rate2(begin, right, most_common, i + 1);
}

int main()
{
  auto binaries = get_data();

  auto rates = get_rates(binaries);
  cout << rates.first * rates.second << endl;

  sort(binaries.begin(), binaries.end());

  int oxygen = stoi(get_rate2(binaries.begin(), binaries.end(), '1'), nullptr, 2);
  int co2 = stoi(get_rate2(binaries.begin(), binaries.end(), '0'), nullptr, 2);

  cout << oxygen * co2 << endl;

  return 0;
}
