#include <array>
#include <map>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

const int P = 10;
typedef array<unsigned char, P> pattern;
const int D = 4;
typedef array<unsigned char, D> digits;
const char SEP = '|';

unsigned char seg_to_bin(string seg)
{
  unsigned char b;
  for (char c : seg)
    b |= 1 << (c - 'a');
  return b;
}
short count_bits(unsigned char bin)
{
  short c = 0;
  for (int i = 1; i < (1 << 7); i <<= 1)
    if (bin & i)
      c++;
  return c;
}

auto get_data()
{
  ifstream input(::input);

  vector<pair<pattern, digits>> res(1);

  int i = 0;
  string w;
  while (input >> w)
  {
    if (w[0] == SEP)
      continue;
    if (i >= P + D)
    {
      i = 0;
      res.push_back({});
    }

    if (i < P)
      res.back().first[i++] = seg_to_bin(w);
    else
      res.back().second[(i++) - P] = seg_to_bin(w);
  }

  input.close();

  return res;
}

map<unsigned char, short> get_mapping(pattern p)
{
  map<unsigned char, short> p2d;
  map<short, unsigned char> d2p;
  // 1 -> 2 char
  // 7 -> 3 char
  // 4 -> 4 char
  // 8 -> 7 char
  vector<unsigned char> six_chars;
  vector<unsigned char> five_chars;
  for (unsigned char seg : p)
  {
    switch (count_bits(seg))
    {
    case 2:
      p2d[seg] = 1;
      d2p[1] = seg;
      break;
    case 3:
      p2d[seg] = 7;
      d2p[7] = seg;
      break;
    case 4:
      p2d[seg] = 4;
      d2p[4] = seg;
      break;
    case 7:
      p2d[seg] = 8;
      d2p[8] = seg;
      break;
    case 6:
      six_chars.push_back(seg);
      break;
    case 5:
      five_chars.push_back(seg);
      break;
    }
  }

  // 9 -> 6 char & [4] == [4]
  // 0 -> 6 char & [1] == [1]
  // 6 -> 6 char
  for (unsigned char seg : six_chars)
    if ((seg & d2p[4]) == d2p[4])
    {
      p2d[seg] = 9;
      d2p[9] = seg;
    }
    else if ((seg & d2p[1]) == d2p[1])
      p2d[seg] = 0;
    else
      p2d[seg] = 6;

  // 3 -> 5 char & [1] == [1]
  // 5 -> 5 char & [9] == 5 char
  // 2 -> 5 char
  for (unsigned char seg : five_chars)
    if ((seg & d2p[1]) == d2p[1])
      p2d[seg] = 3;
    else if ((seg & d2p[9]) == seg)
      p2d[seg] = 5;
    else
      p2d[seg] = 2;

  return p2d;
}

int count_1478(map<unsigned char, short> &mapping, const digits &digits)
{
  int count = 0;
  for (unsigned char d : digits)
  {
    switch (mapping[d])
    {
    case 1:
    case 4:
    case 7:
    case 8:
      ++count;
    }
  }

  return count;
}

int count(map<unsigned char, short> &mapping, const digits &digits)
{
  int count = 0;
  count += mapping[digits[0]] * 1000;
  count += mapping[digits[1]] * 100;
  count += mapping[digits[2]] * 10;
  count += mapping[digits[3]] * 1;
  return count;
}

int main()
{
  auto data = get_data();

  int part1 = 0;
  int part2 = 0;
  for (auto p : data)
  {
    auto mapping = get_mapping(p.first);
    part1 += count_1478(mapping, p.second);
    part2 += count(mapping, p.second);
  }

  cout << part1 << endl;
  cout << part2 << endl;

  return 0;
}
