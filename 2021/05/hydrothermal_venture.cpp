#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <set>
#include <vector>
#include <cassert>

using namespace std;

// const std::string input("sample");
const std::string input("input");

const regex line_re("(\\d+),(\\d+) -> (\\d+),(\\d+)");

struct Point
{
  int x, y;

  bool operator<(const Point &other) const
  {
    if (x == other.x)
      return y < other.y;
    return x < other.x;
  }
  inline bool operator==(const Point &other) const
  {
    return x == other.x && y == other.y;
  }
};

struct Line
{
  Point a, b;

  // top-left, bottom-right
  Point tl, br;

  bool horizontal, vertical;

  Line(Point a, Point b) : a(a),
                           b(b),
                           tl({min(a.x, b.x), min(a.y, b.y)}),
                           br({max(a.x, b.x), max(a.y, b.y)}),
                           horizontal(a.y == b.y),
                           vertical(a.x == b.x)
  {
  }

  set<Point> points()
  {
    if (!_points.empty())
      return _points;

    if (horizontal)
      for (int x = tl.x; x <= br.x; ++x)
        _points.insert({x, a.y});
    else if (vertical)
      for (int y = tl.y; y <= br.y; ++y)
        _points.insert({a.x, y});

    else if (tl == a || tl == b)
      for (int x = tl.x, y = tl.y; x <= br.x; ++x, ++y)
        _points.insert({x, y});
    else
      for (int x = tl.x, y = br.y; x <= br.x; ++x, --y)
        _points.insert({x, y});

    return _points;
  }

private:
  set<Point> _points;
};

struct line_comparer
{
  // x or y
  bool x_axis;
  // top-left or bottom-right
  bool tl;

  constexpr inline int get_coord(const Line &l) const
  {
    const Point &p = tl ? l.tl : l.br;
    return x_axis ? p.x : p.y;
  }

  constexpr bool operator()(const Line &a, const Line &b) const
  {
    return get_coord(a) < get_coord(b);
  }
  constexpr bool operator()(const Line &a, const int &b) const
  {
    return get_coord(a) < b;
  }
  constexpr bool operator()(const int &a, const Line &b) const
  {
    return a < get_coord(b);
  }
};

auto get_data()
{
  ifstream input(::input);

  vector<Line> res;

  string line;
  smatch m;
  while (getline(input, line))
  {
    if (!regex_match(line, m, line_re))
      continue;

    Point p0{stoi(m[1]), stoi(m[2])};
    Point p1{stoi(m[3]), stoi(m[4])};
    res.push_back({p0, p1});
  }

  input.close();

  return res;
}
auto get_straight_lines(const vector<Line> &lines)
{
  vector<Line> res;
  for (Line l : lines)
    if (l.horizontal || l.vertical)
      res.push_back(l);
  return res;
}

set<Point> naive_overlapping_points(
    const vector<Line>::iterator left,
    const vector<Line>::iterator mid,
    const vector<Line>::iterator right,
    bool x_axis)
{
  set<Point> res;
  for (auto a = left; a != mid; ++a)
  {
    sort(mid, right, line_comparer{x_axis, false});
    auto b = lower_bound(mid, right, x_axis ? a->tl.x : a->tl.y, line_comparer{x_axis, false});

    sort(b, right, line_comparer{x_axis, true});
    auto right_end = upper_bound(b, right, x_axis ? a->br.x : a->br.y, line_comparer{x_axis, true});

    for (; b != right_end; ++b)
    {
      set<Point> a_points = a->points();
      set<Point> b_points = b->points();
      set_intersection(
          a_points.begin(), a_points.end(),
          b_points.begin(), b_points.end(),
          inserter(res, res.begin()));
    }
  }

  return res;
}

set<Point> get_overlapping_points(
    const vector<Line>::iterator begin,
    const vector<Line>::iterator end,
    bool x_axis = true)
{
  // Split the vector of lines in half (left-right; or top-bottom)
  // Recursively perform the same operation on the two halves
  // Also count the lines from the left that overlap in the right
  // |----left----|----right----|
  //              |-common-|   <- some lines from left overlapping in right
  //              x1       x2
  if (end - begin <= 1)
    return {};

  sort(begin, end, line_comparer{x_axis, true});
  vector<Line>::iterator mid = begin + (end - begin) / 2;

  set<Point> mid_overlap;

  int x1 = x_axis ? mid->tl.x : mid->tl.y;
  sort(begin, mid, line_comparer{x_axis, false});

  vector<Line>::iterator common_begin = lower_bound(begin, mid, x1, line_comparer{x_axis, false});
  if (mid - common_begin > 0)
  {
    int x2 = x_axis ? (mid - 1)->br.x : (mid - 1)->br.y;
    vector<Line>::iterator common_end = upper_bound(mid, end, x2, line_comparer{x_axis, true});

    mid_overlap = naive_overlapping_points(
        common_begin,
        mid,
        common_end,
        !x_axis);
  }

  set<Point> left_overlap = get_overlapping_points(begin, mid, !x_axis);
  set<Point> right_overlap = get_overlapping_points(mid, end, !x_axis);

  set_union(left_overlap.begin(), left_overlap.end(),
            right_overlap.begin(), right_overlap.end(),
            inserter(mid_overlap, mid_overlap.begin()));

  return mid_overlap;
}

int main()
{
  auto lines = get_data();
  auto straight_lines = get_straight_lines(lines);

  auto overlapping = get_overlapping_points(straight_lines.begin(), straight_lines.end());
  cout << overlapping.size() << endl;

  overlapping = get_overlapping_points(lines.begin(), lines.end());
  cout << overlapping.size() << endl;
  return 0;
}
