#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

vector<pair<char, int>> get_data()
{
  ifstream input(::input);

  vector<pair<char, int>> commands;

  string cmd;
  int n;
  while (input >> cmd >> n)
  {
    commands.push_back({cmd[0], n});
  }
  input.close();

  return commands;
}

pair<int, int> get_depth_and_position(const vector<pair<char, int>> &cmds, bool part2 = false)
{
  int depth = 0, position = 0, aim = 0;

  for (auto cmd : cmds)
  {
    switch (cmd.first)
    {
    case 'f':
      position += cmd.second;
      if (part2)
        depth += cmd.second * aim;
      break;
    case 'd':
      if (part2)
        aim += cmd.second;
      else
        depth += cmd.second;
      break;
    case 'u':
      if (part2)
        aim -= cmd.second;
      else
        depth -= cmd.second;
      break;
    }
  }

  return {depth, position};
}

int main()
{
  auto commands = get_data();

  pair<int, int> dnp = get_depth_and_position(commands);
  cout << dnp.first * dnp.second << endl;

  dnp = get_depth_and_position(commands, true);
  cout << dnp.first * dnp.second << endl;

  return 0;
}
