#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  vector<int> res;

  int n;
  char c;
  while (input >> n >> c)
    res.push_back(n);
  res.push_back(n);

  input.close();

  return res;
}

int get_fuel(const vector<int> &crabs, int pos, bool constant)
{
  int fuel = 0;
  for (int crab : crabs)
  {
    int f = abs(crab - pos);
    if (!constant)
      // triangular number sequence
      f = (f * (f + 1)) / 2;
    fuel += f;
  }

  return fuel;
}

int find_min_fuel(const vector<int> &crabs, int min, int max, bool constant)
{
  if (min + 1 == max)
    return get_fuel(crabs, min, constant);

  int mid = min + (max - min) / 2;

  if (get_fuel(crabs, mid - 1, constant) < get_fuel(crabs, mid, constant))
    // ascending
    return find_min_fuel(crabs, min, mid, constant);
  else
    // descending
    return find_min_fuel(crabs, mid, max, constant);
}

int main()
{
  auto crabs = get_data();
  sort(crabs.begin(), crabs.end());

  cout << find_min_fuel(crabs, crabs.front(), crabs.back(), true) << endl;
  cout << find_min_fuel(crabs, crabs.front(), crabs.back(), false) << endl;

  return 0;
}
