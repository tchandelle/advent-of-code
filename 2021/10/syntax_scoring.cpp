#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <stack>
#include <tuple>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  vector<string> res;
  string s;
  while (getline(input, s))
    res.push_back(s);

  return res;
}

pair<string, char> autocomplete(string l)
{
  stack<char> q;
  for (char c : l)
  {
    switch (c)
    {
    // opening and closing separated by 2, except parenthesis
    case '(':
      --c;
    case '[':
    case '{':
    case '<':
      q.push(c);
      break;

    default:
      if (q.size() == 0)
        return {"", c};

      char opening = q.top();
      if (opening + 2 != c)
        return {"", c};

      q.pop();
      break;
    }
  }

  string autocomplete;
  while (!q.empty())
  {
    autocomplete += (q.top() + 2);
    q.pop();
  }

  return {autocomplete, 0};
}

int get_score(char c)
{
  switch (c)
  {
  case ')':
    return 3;
  case ']':
    return 57;
  case '}':
    return 1197;
  case '>':
    return 25137;
  }

  return 0;
}
unsigned long long get_score(string s)
{
  unsigned long long score = 0;
  for (char c : s)
  {
    score *= 5;
    switch (c)
    {
    case ')':
      score += 1;
      break;
    case ']':
      score += 2;
      break;
    case '}':
      score += 3;
      break;
    case '>':
      score += 4;
      break;
    }
  }

  return score;
}

int main()
{
  auto lines = get_data();

  int score1 = 0;
  vector<unsigned long long> score2;
  char illegal;
  string autoc;
  for (string l : lines)
  {
    tie(autoc, illegal) = autocomplete(l);
    if (illegal)
      score1 += get_score(illegal);
    else
      score2.push_back(get_score(autoc));
  }
  sort(score2.begin(), score2.end());

  cout << score1 << endl;
  cout << score2[score2.size() / 2] << endl;
}
