#include <fstream>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

auto get_data()
{
  ifstream input(::input);

  vector<string> res;
  string s;
  while (getline(input, s))
    res.push_back(s);

  return res;
}

int get_bassin(const vector<string> &height_map, int x, int y)
{
  set<pair<int, int>> bassin;

  queue<pair<int, int>> q;
  q.push({x, y});

  while (!q.empty())
  {
    pair<int, int> v = q.front();
    q.pop();
    x = v.first;
    y = v.second;

    if (height_map[y][x] >= '9')
      continue;

    bassin.insert(v);

    if (x > 0 && height_map[y][x] < height_map[y][x - 1])
      q.push({x - 1, y});
    if (x < height_map[y].size() - 1 && height_map[y][x] < height_map[y][x + 1])
      q.push({x + 1, y});
    if (y > 0 && height_map[y][x] < height_map[y - 1][x])
      q.push({x, y - 1});
    if (y < height_map.size() - 1 && height_map[y][x] < height_map[y + 1][x])
      q.push({x, y + 1});
  }

  return bassin.size();
}

pair<int, int> get_risk_and_bassins(const vector<string> &height_map)
{
  int risk = 0;
  priority_queue<int, vector<int>, greater<int>> bassins;
  for (int i = 0; i < 3; ++i)
    bassins.push(0);
  for (int y = 0; y < height_map.size(); ++y)
  {
    string line = height_map[y];
    for (int x = 0; x < line.size(); ++x)
    {
      if (x > 0 && line[x] >= line[x - 1])
        continue;
      if (x < line.size() - 1 && line[x] >= line[x + 1])
        continue;
      if (y > 0 && line[x] >= height_map[y - 1][x])
        continue;
      if (y < height_map.size() - 1 && line[x] >= height_map[y + 1][x])
        continue;
      risk += line[x] - '0' + 1;
      bassins.push(get_bassin(height_map, x, y));
      bassins.pop();
    }
  }

  int product = 1;
  while (!bassins.empty())
  {
    product *= bassins.top();
    bassins.pop();
  }

  return {risk, product};
}

int main()
{
  auto height_map = get_data();

  auto rb = get_risk_and_bassins(height_map);
  cout << rb.first << endl;
  cout << rb.second << endl;

  return 0;
}
