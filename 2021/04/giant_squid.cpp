#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

// const std::string input("sample");
const std::string input("input");

const size_t bingo_size = 5 * 5;
typedef array<int, bingo_size> bingo;

const int winners[] = {
    0b1111100000000000000000000,
    0b0000011111000000000000000,
    0b0000000000111110000000000,
    0b0000000000000001111100000,
    0b0000000000000000000011111,

    0b1000010000100001000010000,
    0b0100001000010000100001000,
    0b0010000100001000010000100,
    0b0001000010000100001000010,
    0b0000100001000010000100001};

pair<vector<int>, vector<bingo>> get_data()
{
  ifstream input(::input);

  vector<int> numbers;
  int n;
  char comma;
  do
  {
    input >> n >> comma;
    numbers.push_back(n);
  } while (comma == ',');

  // seek back 1 char
  input.seekg(-1, ios_base::cur);

  vector<bingo> bingos(1);
  int i = 0;
  while (input >> n)
  {
    if (i >= bingo_size)
    {
      bingos.push_back(bingo{});
      i = 0;
    }
    bingos.back()[i++] = n;
  }

  input.close();

  return {numbers, bingos};
}

bool is_winning(int checked)
{
  for (int w : winners)
  {
    if ((checked & w) == w)
      return true;
  }
  return false;
}

int compute_score(bingo b, int checked, int last)
{
  int sum = 0;
  for (int i = 0; i < b.size(); ++i)
  {
    if ((checked & (1 << i)) == 0)
      sum += b[i];
  }

  return sum * last;
}

vector<int> play(vector<int> numbers, vector<bingo> bingos)
{
  vector<int> checked(bingos.size());

  vector<int> winners;
  winners.reserve(bingos.size());

  for (int n : numbers)
  {
    for (int i = bingos.size() - 1; i >= 0; --i)
    {
      bingo bingo_card = bingos[i];

      auto checked_id = find(bingo_card.begin(), bingo_card.end(), n) - bingo_card.begin();
      if (checked_id >= bingo_size)
        continue;
      checked[i] |= 1 << checked_id;

      if (is_winning(checked[i]))
      {
        winners.push_back(compute_score(bingo_card, checked[i], n));

        // remove bingo card
        swap(bingos.back(), bingos[i]);
        bingos.pop_back();

        swap(checked.back(), checked[i]);
        checked.pop_back();
      }
    }
  }

  return winners;
}

int main()
{
  auto data = get_data();

  vector<int> winning_scores = play(data.first, data.second);

  cout << winning_scores.front() << endl;
  cout << winning_scores.back() << endl;

  return 0;
}
