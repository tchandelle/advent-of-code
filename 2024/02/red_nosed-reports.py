#!/usr/bin/python3

from itertools import pairwise

def is_safe(report):
    diff = [ b - a for a, b in pairwise(report) ]
    return (all(a >= 1 and a <= 3 for a in diff) or
            all(a <= -1 and a >= -3 for a in diff))

def is_tolerable(r):
    return any(
        is_safe(r[:i] + r[i+1:])
        for i, _ in enumerate(r)
    )


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        reports = [ [int(l) for l in line.split()] for line in input_file ]

    # Part 1
    safe = 0
    for report in reports:
        if is_safe(report):
            safe += 1

    print(safe)

    # Part 2
    tolerable = 0
    for report in reports:
        if is_safe(report) or is_tolerable(report):
            tolerable += 1

    print(tolerable)


if __name__ == "__main__":
    main()
