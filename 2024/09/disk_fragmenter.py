#!/usr/bin/python3

def parse_disk_content_uncompressed(str):
    disk = []
    is_file = False
    id = 0
    for c in str:
        is_file = not is_file

        if is_file:
            disk += int(c) * [id]
            id += 1
        else:
            disk += int(c) * [None]

    return disk

def decompress_disk(disk: list):
    disk_ret = []
    for id, length in disk:
        disk_ret += [id] * length
    return disk_ret

def parse_disk_content_compressed(str):
    disk = []
    is_file = False
    id = 0
    for c in str:
        length = int(c)
        is_file = not is_file

        if is_file:
            disk.append((id, length))
            id += 1
        else:
            disk.append((None, length))

    return disk



def defrag1(disk):
    for idx, id in enumerate(disk):
        if id is None:
            while (last_block := disk.pop()) is None:
                pass
            disk[idx] = last_block

def defrag2(disk: list):
    idx = len(disk) - 1
    while idx >= 0:
        id, length = disk[idx]
        if id is None:
            idx -= 1
            continue

        free_space = next(((id, free_length) for id, free_length in disk[:idx] if id is None and length <= free_length), None)
        if free_space is not None:
            free_space_idx = disk.index(free_space)
            disk[free_space_idx] = (None, free_space[1] - length)
            disk.insert(free_space_idx, (id, length))
            disk[idx + 1] = (None, length)
        else:
            idx -= 1


def checksum(disk):
    return sum(idx * id for idx, id in enumerate(disk) if id is not None)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        input = input_file.readline()
        disk1 = parse_disk_content_uncompressed(input)
        disk2 = parse_disk_content_compressed(input)

    # Part 1
    defrag1(disk1)
    print(checksum(disk1))

    # Part 2
    defrag2(disk2)
    print(checksum(decompress_disk(disk2)))



if __name__ == "__main__":
    main()
