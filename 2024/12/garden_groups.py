#!/usr/bin/python3

DIRS = [1, -1, 1j, -1j]
HORIZONTALS = [1, -1]
VERTICALS = [1j, -1j]
CORNERS = [0.5+0.5j, -0.5+0.5j, -0.5-0.5j, 0.5-0.5j]

def extract_region(garden: dict[complex, str]):
    pos = next(iter(garden.keys()))
    id = garden[pos]
    todo = [pos]
    visited = set[complex]()

    fences = set[complex]()
    area = 0

    while len(todo) > 0:
        pos = todo.pop()
        if pos not in visited:
            visited.add(pos)
            garden.pop(pos)
            area += 1
            for dir in DIRS:
                neighbor = pos + dir
                if neighbor not in visited and neighbor in garden and garden[neighbor] == id:
                    todo.append(neighbor)
                elif neighbor not in visited:
                    fences.add(pos + dir / 2)

    perimeter1 = len(fences)

    perimeter2 = 0

    for fence in fences:
        dirs = HORIZONTALS if fence.real == int(fence.real) else VERTICALS
        straights = sum(
            1 for dir in dirs
            if (fence + dir) in fences
        )
        corners = sum(
            1 for corner in CORNERS
            if (fence + corner) in fences
        )
        # corner-cases
        match (straights, corners):
            case (1, 1) | (2, 2):
                perimeter2 += 0.5
            case (0, 2) | (1, 3):
                perimeter2 += 1

    return area, perimeter1, int(perimeter2)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        garden = {
            complex(x, y): c
            for y, line in enumerate(input_file)
            for x, c in enumerate(line.strip())
        }

    # Part 1 & 2
    price1 = 0
    price2 = 0
    while len(garden) > 0:
        area, perimeter1, perimeter2 = extract_region(garden)
        price1 += area * perimeter1
        price2 += area * perimeter2
    print(price1)
    print(price2)


if __name__ == "__main__":
    main()
