#!/usr/bin/python3

import functools
import operator

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        pairs = [ line.strip().split('   ') for line in input_file ]

        l0 = [ int(p[0]) for p in pairs ]
        l1 = [ int(p[1]) for p in pairs ]

    l0.sort()
    l1.sort()

    # Part 1
    distances = [ abs(l0[i] - l1[i]) for i, _ in enumerate(l0) ]
    print(functools.reduce(operator.add, distances, 0))

    # Part 2
    similarity = [
        num * l1.count(num)
        for num in l0
    ]
    print(functools.reduce(operator.add, similarity, 0))


if __name__ == "__main__":
    main()
