#!/usr/bin/python3

# Part 1 - with X as (0+0j)
xmas_patterns = [
  { 1+0j: 'M', 2+0j: 'A', 3+0j: 'S' },
  { -1+0j: 'M', -2+0j: 'A', -3+0j: 'S' },

  { 0+1j: 'M', 0+2j: 'A', 0+3j: 'S' },
  { 0-1j: 'M', 0-2j: 'A', 0-3j: 'S' },

  { 1+1j: 'M', 2+2j: 'A', 3+3j: 'S' },
  { 1-1j: 'M', 2-2j: 'A', 3-3j: 'S' },
  { -1+1j: 'M', -2+2j: 'A', -3+3j: 'S' },
  { -1-1j: 'M', -2-2j: 'A', -3-3j: 'S' },
]

# Part 2 - with A as (0+0j)
x_mas_patterns = [
  { -1-1j: 'M', +1+1j: 'S', -1+1j: 'M', +1-1j: 'S' },
  { -1-1j: 'S', +1+1j: 'M', -1+1j: 'M', +1-1j: 'S' },
  { -1-1j: 'M', +1+1j: 'S', -1+1j: 'S', +1-1j: 'M' },
  { -1-1j: 'S', +1+1j: 'M', -1+1j: 'S', +1-1j: 'M' },
]

def count(grid, pos, origin, patterns):
    if grid[pos] != origin:
        return 0

    matches = 0
    for pattern in patterns:
        matches += 1
        for rel, c in pattern.items():
            p = pos + rel
            if p not in grid or grid[p] != c:
                matches -= 1
                break

    return matches

def count_xmas(grid, pos):
    return count(grid, pos, 'X', xmas_patterns)
def count_x_mas(grid, pos):
    return count(grid, pos, 'A', x_mas_patterns)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        grid = {
            complex(x, y): c
            for y, line in enumerate(input_file)
            for x, c in enumerate(line)
        }

    # Part 1
    print(sum(count_xmas(grid, pos) for pos in grid))

    # Part 2
    print(sum(count_x_mas(grid, pos) for pos in grid))


if __name__ == "__main__":
    main()
