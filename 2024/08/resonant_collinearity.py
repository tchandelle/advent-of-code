#!/usr/bin/python3

from itertools import permutations

def get_antinodes_for(grid, frequency):
    coords = [c for c, f in grid.items() if f == frequency]

    antinodes1 = set()
    antinodes2 = set()

    for a, b in permutations(coords, 2):
        dist = b - a
        antinodes2.add(b)
        part1 = True
        while (b := b + dist) in grid:
            if part1:
                antinodes1.add(b)
                part1 = False
            antinodes2.add(b)


    return antinodes1, antinodes2

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        grid = {
            complex(x, y): c
            for y, line in enumerate(input_file)
            for x, c in enumerate(line.strip())
        }
        frequencies = set(grid.values()) - set('.')

    antinodes1 = set()
    antinodes2 = set()
    for f in frequencies:
        a1, a2 = get_antinodes_for(grid, f)
        antinodes1 |= a1
        antinodes2 |= a2

    # Part 1
    print(len(antinodes1))
    # Part 2
    print(len(antinodes2))

if __name__ == "__main__":
    main()
