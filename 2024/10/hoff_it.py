#!/usr/bin/python3

from functools import cache

with open('input', 'r', encoding='utf-8') as input_file:
    grid = {
        complex(x, y): int(c)
        for y, line in enumerate(input_file)
        for x, c in enumerate(line.strip())
    }

def score_trailhead(pos: complex):
    nines, trails = find_top(pos)
    return len(nines), trails



@cache
def find_top(pos: complex):
    if grid[pos] == 9:
        return set([pos]), 1

    nines = set()
    trails = 0
    steps = [pos + 1, pos - 1, pos + 1j, pos - 1j]
    for step in steps:
        if step in grid and grid[step] == grid[pos] + 1:
            nine, trail = find_top(step)
            nines |= nine
            trails += trail

    return nines, trails


def main():
    scores = [score_trailhead(c) for c, height in grid.items() if height == 0]

    # Part 1
    print(sum(nines for nines, _ in scores))

    # Part 2
    print(sum(trails for _, trails in scores))


if __name__ == "__main__":
    main()
