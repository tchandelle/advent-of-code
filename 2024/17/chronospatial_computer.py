#!/usr/bin/python3


class Computer:
    a = 0
    b = 0
    c = 0

    ptr = 0
    prog = []
    out = []

    def __init__(self, a, b, c, prog):
        self.a = a
        self.b = b
        self.c = c
        self.prog = prog
        self.out = []

    def get_combo(self, operand):
        match operand:
            case 4:
                return self.a
            case 5:
                return self.b
            case 6:
                return self.c
            case 7:
                # reserved
                return 0
            case _:
                return operand

    def exec(self):
        opcode = self.prog[self.ptr]
        operand = self.prog[self.ptr + 1]
        combo = self.get_combo(operand)
        match opcode:
            case 0: # adv
                self.a = int(self.a / pow(2, combo))
            case 6: # bdv
                self.b = int(self.a / pow(2, combo))
            case 7: # cdv
                self.c = int(self.a / pow(2, combo))

            case 1: # bxl
                self.b = self.b ^ operand
            case 4: # bxc
                self.b = self.b ^ self.c

            case 2: # bst
                self.b = combo % 8
            case 5: # out
                self.out.append(combo % 8)

            case 3: # jnz
                if self.a != 0:
                    self.ptr = operand
                    return

        self.ptr += 2

    def run(self):
        while self.ptr < len(self.prog):
            self.exec()


# c = Computer(0, 0, 9, [2,6])
# c.run()
# assert(c.b == 1)

# c = Computer(10, 0, 0, [5,0,5,1,5,4])
# c.run()
# assert(c.out == [0,1,2])

# c = Computer(2024, 0, 0, [0,1,5,4,3,0])
# c.run()
# assert(c.a == 0)
# assert(c.out == [4,2,5,6,7,7,7,7,3,1,0])

# c = Computer(0, 29, 0, [1,7])
# c.run()
# assert(c.b == 26)

# c = Computer(0, 2024, 43690, [4,0])
# c.run()
# assert(c.b == 44354)

# c = Computer(729, 0, 0, [0,1,5,4,3,0])
# c.run()
# assert(c.out == [4,6,3,5,6,3,5,2,1,0])

prog = [] # Input here

# Part 1
c = Computer(0, 0, 0, prog)
c.run()

print(','.join(str(c) for c in c.out))

# Part 2

def solve(aa: range, i: int):
    for a in aa:
        cmp = Computer(a, 0, 0, prog)
        cmp.run()

        if cmp.out == prog[i:]:
            if i == 0:
                return a
            
            solved = solve(range(a * 8, (a + 1) * 8), i - 1)
            if solved is not None:
                return solved

print(solve(range(1, 8), len(prog) - 1))
