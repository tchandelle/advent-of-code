#!/usr/bin/python3

from functools import reduce
from operator import mul
import re

def get_quadrants(size: complex):
    vert = int((size.real - 1) / 2)
    hor = int((size.imag - 1) / 2)

    return (
        set(complex(x, y) for x in range(0, vert) for y in range(0, hor)),
        set(complex(x, y) for x in range(vert + 1, int(size.real)) for y in range(0, hor)),
        set(complex(x, y) for x in range(vert + 1, int(size.real)) for y in range(hor + 1, int(size.imag))),
        set(complex(x, y) for x in range(0, vert) for y in range(hor + 1, int(size.imag)))
    )

# SIZE = 11+7j
SIZE = 101+103j
QUADRANTS = get_quadrants(SIZE)

PATTERN = r'^p=(\d+),(\d+) v=(-?\d+),(-?\d+)$'

def move(robot: tuple[complex, complex], sec: int):
    pos, vec = robot
    pos += vec * sec
    # pos %= SIZE
    return complex(
        pos.real % SIZE.real,
        pos.imag % SIZE.imag
    )

def print_robots(robots: list[complex]):
    for y in range(int(SIZE.imag)):
        for x in range(int(SIZE.real)):
            pos = complex(x, y)
            print('#' if pos in robots else '.', end='')
        print('')
    print('')


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        robots = [
            (
                complex(int(m[1]), int(m[2])),
                complex(int(m[3]), int(m[4]))
            )
            for line in input_file
            for m in [re.match(PATTERN, line)]
        ]

    # Part 1
    robots_positions = [move(robot, 100) for robot in robots]
    print(reduce(mul, (sum(r in q for r in robots_positions) for q in QUADRANTS)))

    # Part 2
    for i in range(int(SIZE.real * SIZE.imag)):
        robots_positions = [move(robot, i) for robot in robots]
        per_quad = list(sum(r in q for r in robots_positions) for q in QUADRANTS)

        # When most of the robots are in one quadrant,
        # print them to visualize it.
        # '300' was determined by trial and error.
        # Initially did '200' and look through all of them (107)
        if any(num > 300 for num in per_quad):
            print(i)
            print_robots(robots_positions)


if __name__ == "__main__":
    main()
