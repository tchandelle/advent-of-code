#!/usr/bin/python3

from functools import cache
from itertools import pairwise

KEYS = {
    '7': (0, 0), '8': (1, 0), '9': (2, 0),
    '4': (0, 1), '5': (1, 1), '6': (2, 1),
    '1': (0, 2), '2': (1, 2), '3': (2, 2),
                 '0': (1, 3), 'A': (2, 3),

                 '^': (1, 3), 'A': (2, 3),
    '<': (0, 4), 'v': (1, 4), '>': (2, 4),
}

@cache
def move(a, b):
    a, b = KEYS[a], KEYS[b]

    h, v = a[0] - b[0], a[1] - b[1]

    h = h * '<' if h > 0 else -h * '>'
    v = v * '^' if v > 0 else -v * 'v'

    # Avoid forbidden space
    if a[1] == 3 and b[0] == 0:
        return v + h + 'A'
    if a[0] == 0 and b[1] == 3:
        return h + v + 'A'

    if h.startswith('<'):
        return h + v + 'A'
    else:
        return v + h + 'A'

@cache
def type(code: str):
    mid = int(len(code) / 2)
    a_idx = code[mid:].index('A') + mid + 1

    left, right = code[:a_idx], code[a_idx:]

    if len(right) == 0:
        return ''.join(move(a, b) for a, b in pairwise('A' + left))

    return type(left) + type(right)

def type_n_level(code: str, n: int):
    for _ in range(n):
        code = type(code)
    return code

CODES = [
    '029A',
    '980A',
    '179A',
    '456A',
    '379A',
]

# Split in half.
# - Expand the code to the first half-ish layer of robots
# - Re-expand the code, segment by segment, the second half layer
def find_complexity(r0, r1):
    complexity = 0

    for code in CODES:
        num_code = int(code[:-1])
        code = type(code)

        len_code = 0
        code = type_n_level(code, r0)

        for c2 in code.split('A')[:-1]:
            len_code += len(type_n_level(c2 + 'A', r1))

        complexity += num_code * len_code
    return complexity

# Part 1
print(find_complexity(1, 1))

# Part 2
print(find_complexity(10, 15))
