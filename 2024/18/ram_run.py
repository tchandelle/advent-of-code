#!/usr/bin/python3

from cmath import inf
from collections import defaultdict
from heapq import heappop, heappush

VEC = [
    +1+0j, # east
    +0+1j, # south
    -1+0j, # west
    +0-1j, # north
]

SIZE = 70

START = 0+0j
END = SIZE+SIZE * 1j

def navigate(bytes: list[complex], start: complex, end: complex):
    # Counter used in the heap to avoid comparing complex
    c = 0

    dist = defaultdict(lambda: inf)
    q = []

    u = start

    heappush(q, (0, c, u))
    dist[u] = 0

    while len(q):
        u_dist, _, u = heappop(q)

        if u == end:
            return u_dist

        for vec in VEC:
            v = u + vec

            alt = u_dist + 1
            if (v.imag >= 0 and v.real >= 0
                    and v.imag <= SIZE and v.real <= SIZE
                    and v not in bytes
                    and alt < dist[v]):
                dist[v] = alt
                c += 1
                heappush(q, (dist[v], c, v))

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        bytes = [
            complex(int(xy[0]), int(xy[1]))
            for line in input_file.readlines()
            for xy in [line.split(',')]
        ]

    # Part 1
    print(navigate(set(bytes[:1024]), START, END))

    # Part 2
    left, right = 1024, len(bytes)
    while right - left > 1:
        mid = int(left + (right - left) / 2)

        if navigate(set(bytes[:mid]), START, END) is None:
            right = mid
        else:
            left = mid

    print('{x},{y}'.format(x=int(bytes[left].real), y=int(bytes[left].imag)))


if __name__ == "__main__":
    main()
