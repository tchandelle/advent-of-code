#!/usr/bin/python3

VEC = {
    '>': +1+0j,
    '<': -1+0j,
    '^': +0-1j,
    'v': +0+1j,
}

def do_move(warehouse, moving):
    # Remove duplicate instructions.
    # But order is important, so can't use a set
    for obj, dest in list(dict.fromkeys(moving)):
        warehouse[dest] = warehouse[obj]
        warehouse.pop(obj)


def can_move(warehouse: dict[complex, str], obj: complex, vec: complex):
    dest = obj + vec
    if dest not in warehouse:
        return [(obj, dest)]
    
    match warehouse[dest]:
        case 'O':
            if (moving := can_move(warehouse, dest, vec)) is not None:
                return moving + [(obj, dest)]
        case '[' | ']':
            if vec == VEC['<'] or vec == VEC['>']:
                if (moving := can_move(warehouse, dest, vec)) is not None:
                    return moving + [(obj, dest)]
            else:
                d1, d2 = (dest, dest + 1) if warehouse[dest] == '[' else (dest - 1, dest)

                moving1 = can_move(warehouse, d1, vec)
                moving2 = can_move(warehouse, d2, vec)

                if moving1 is not None and moving2 is not None:
                    return moving1 + moving2 + [(obj, dest)]

    return None


def gps(pos: complex):
    return int(pos.imag * 100 + pos.real)

def print_warehouse(warehouse: dict[complex, str]):
    y_max = int(max(k.imag for k in warehouse.keys())) + 1
    x_max = int(max(k.real for k in warehouse.keys())) + 1

    for y in range(y_max):
        for x in range(x_max):
            pos = complex(x, y)
            print(warehouse[pos] if pos in warehouse else '.', end='')
        print('')
    print('')


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        lines = input_file.readlines()
        middle = lines.index('\n')

        warehouse1 = {
            complex(x, y): c
            for y, line in enumerate(lines[:middle])
            for x, c in enumerate(line.strip())
            if c != '.'
        }

        warehouse2 = {
            p: c
            for y, line in enumerate(lines[:middle])
            for x, c in enumerate(line.strip())
            for p, c in {
                complex(x * 2, y): '[' if c == 'O' else c,
                complex(x * 2 + 1, y): ']' if c == 'O' else c
            }.items()
            if c != '.'
        }

        instructions = [
            c
            for line in lines[middle+1:]
            for c in line.strip()
        ]

    # Part 1
    robot_idx = list(warehouse1.values()).index('@')
    robot_pos = list(warehouse1.keys())[robot_idx]

    for instruction in instructions:
        moving = can_move(warehouse1, robot_pos, VEC[instruction])
        if moving is not None:
            do_move(warehouse1, moving)
            robot_pos = moving[-1][1]

    print(sum(gps(pos) for pos, c in warehouse1.items() if c == 'O'))

    # Part 2
    robot_idx = list(warehouse2.values()).index('@')
    robot_pos = list(warehouse2.keys())[robot_idx]
    warehouse2.pop(robot_pos + 1)

    for instruction in instructions:
        moving = can_move(warehouse2, robot_pos, VEC[instruction])
        if moving is not None:
            do_move(warehouse2, moving)
            robot_pos = moving[-1][1]

    print(sum(gps(pos) for pos, c in warehouse2.items() if c == '['))

if __name__ == "__main__":
    main()
