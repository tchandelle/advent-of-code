#!/usr/bin/python3

import re

# Right to left, with subtraction and division
def is_true_equation(equation, with_concat = False):
    b, a = equation.pop(), equation.pop()

    if len(equation) == 0:
        return b == a

    sub = b - a
    if sub >= 0 and is_true_equation(equation + [sub], with_concat):
        return True

    if (with_concat
            and b != a and str(b).endswith(str(a))
            and is_true_equation(equation + [int(str(b)[:-len(str(a))])], True)):
        return True

    return (b % a) == 0 and is_true_equation(equation + [int(b / a)], with_concat)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        equations = [
            list(map(int, re.split(r':? ', line)))
            for line in input_file
        ]
        # Move result to last position
        for equation in equations:
            equation.append(equation.pop(0))

    # Part 1
    print(sum(
        equation[-1]
            if is_true_equation(equation.copy(), False)
            else 0
        for equation in equations
    ))

    # Part 2
    print(sum(
        equation[-1]
            if is_true_equation(equation.copy(), True)
            else 0
        for equation in equations
    ))


if __name__ == "__main__":
    main()
