#!/usr/bin/python3

from re import findall


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        mul = findall(r'(do|don\'t)\(\)|mul\((\d+),(\d+)\)', input_file.read())

    # Part 1
    print(sum(int(x) * int(y) if x.isdigit() else 0 for _, x, y in mul))

    # Part 2
    doing = True
    sigma = 0

    for do_or_dont, x, y in mul:
        match do_or_dont:
            case 'do':
                doing = True
            case 'don\'t':
                doing = False
            case _:
                sigma += int(x) * int(y) if doing else 0

    print(sigma)

if __name__ == "__main__":
    main()
