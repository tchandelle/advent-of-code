#!/usr/bin/python3

DIR = [
    +0-1j, # UP
    +1+0j, # RIGHT
    +0+1j, # DOWN
    -1+0j, # LEFT
]

def patrol(lab, guard_pos):
    dir = DIR[0]

    visited = set([guard_pos])
    visited_per_dir = { dir: set() for dir in DIR }
    visited_per_dir[dir].add(guard_pos)

    while (next_pos := guard_pos + dir) in lab:
        match lab[next_pos]:
            case '#':
                dir = DIR[(DIR.index(dir) + 1) % 4]
            case _:
                visited.add(next_pos)

                # Loop detected
                if next_pos in visited_per_dir[dir]:
                    return visited, True
                visited_per_dir[dir].add(next_pos)

                guard_pos = next_pos

    return visited, False

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        grid = {
            complex(x, y): c
            for y, line in enumerate(input_file)
            for x, c in enumerate(line.strip())
        }

    guard_idx = list(grid.values()).index('^')
    guard_pos = list(grid.keys())[guard_idx]

    # Part 1
    visited, _ = patrol(grid, guard_pos)
    print(len(visited))

    # Part 2
    c = 0
    for pos in visited:
        if pos == guard_pos:
            continue

        grid_with_obstruction = grid.copy()
        grid_with_obstruction[pos] = '#'

        _, loop = patrol(grid_with_obstruction, guard_pos)
        if loop:
            c += 1

    print(c)


if __name__ == "__main__":
    main()
