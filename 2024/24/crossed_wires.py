#!/usr/bin/python3

with open('input', 'r', encoding='utf-8') as input_file:
    lines = input_file.readlines()
    middle = lines.index('\n')

    registers = {
        split[0]: split[1] == '1'
        for line in lines[:middle]
        for split in [line.strip().split(': ')]
    }

    operations = {
        split[1]: tuple(split[0].split(' '))
        for line in lines[middle+1:]
        for split in [line.strip().split(' -> ')]
    }

def solve(var: str, registers: dict, operations: dict):
    if var in registers:
        return registers[var]
    
    if var not in operations:
        return False

    a, op, b = operations[var]
    a = solve(a, registers, operations)
    b = solve(b, registers, operations)
    match op:
        case 'AND':
            registers[var] = a and b
        case 'OR':
            registers[var] = a or b
        case 'XOR':
            registers[var] = a ^ b
    return registers[var]

def get_binary(prefix, registers: dict, operations: dict):
    return ''.join(
        '1' if solve('{}{:02}'.format(prefix, i), registers, operations) else '0'
        for i in reversed(range(64))
    )

# Part 1
print(int(get_binary('z', registers, operations), 2))
