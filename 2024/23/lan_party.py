#!/usr/bin/python3

from collections import defaultdict


with open('input', 'r', encoding='utf-8') as input_file:
    one_to_n = defaultdict[tuple, set[tuple]](lambda: set[tuple]())
    for line in input_file:
        parts = line.strip().split('-')
        one_to_n[tuple([ parts[0] ])].add(tuple([ parts[1] ]))
        one_to_n[tuple([ parts[1] ])].add(tuple([ parts[0] ]))

def find_triplets(connections: dict[tuple, set[tuple]]):
    triplets = set()

    for a, bb in connections.items():
        if a[0].startswith('t'):
            for b in bb:
                for c in connections[a].intersection(connections[b]):
                    triplets.add(tuple(sorted(a + b + c)))

    return triplets

# Part 1
print(len(find_triplets(one_to_n)))

# Part 2

def expand_clusters(clusters: dict[tuple, set[tuple]], one_to_n: dict[tuple, set[tuple]], expected_size: int):
    next_groups = {}
    for t, s in clusters.items():
        for computer in s:
            if len(s.intersection(one_to_n[computer])) == expected_size:
                next_groups[tuple(sorted(t + computer))] = s.intersection(one_to_n[computer])

    return next_groups

# By analyzing the input, we see that every computer is
# connected to 13 other. Then, it's a fair assumption
# to consider 13 as the size of the biggest cluster to find
N = 13

next_clusters = one_to_n
for i in range(1, N):
    next_clusters = expand_clusters(next_clusters, one_to_n, N - i - 1)

assert(len(next_clusters) == 1)
password = next(iter(next_clusters))
print(','.join(password))
