#!/usr/bin/python3

from collections import defaultdict
from heapq import heappop, heappush
from math import inf

with open('input', 'r', encoding='utf-8') as input_file:
    racetrack = {
        complex(x, y): c
        for y, line in enumerate(input_file.readlines())
        for x, c in enumerate(line.strip())
    }
    start_idx = list(racetrack.values()).index('S')
    end_idx = list(racetrack.values()).index('E')
    start = list(racetrack.keys())[start_idx]
    end = list(racetrack.keys())[end_idx]
    racetrack[start] = racetrack[end] = '.'

VEC = [
    +1+0j, # east
    +0+1j, # south
    -1+0j, # west
    +0-1j, # north
]

def generate_steps(i: int):
    pos = {
        0+0j: 0
    }

    q = [0+0j]
    while len(q):
        u = q.pop(0)
        if pos[u] >= i:
            continue

        for v in VEC:
            if u + v not in pos:
                pos[u+v] = pos[u] + 1
                q.append(u+v)

    return pos

def race(track: dict[complex, str], start: complex, end: complex):
    dist = defaultdict(lambda: inf)
    q = []

    u = start
    heappush(q, (0, u))
    dist[u] = 0

    while len(q):
        u_dist, u = heappop(q)

        if u == end:
            return dist

        for vec in VEC:
            v = u + vec
            alt = u_dist + 1

            if track[v] == '.' and alt < dist[v]:
                dist[v] = alt
                heappush(q, (dist[v], v))

def get_cheats(dist_track, saving, vectors):
    cheats = set()
    for pos, dist in dist_track.items():
        for vec, cheat in vectors.items():
            next_pos = pos + vec
            if next_pos in dist_track:
                saved = dist_track[next_pos] - dist - cheat
                if saved >= saving:
                    cheats.add((pos, next_pos))
    return cheats

dist = race(racetrack, start, end)

# Part 1
print(len(get_cheats(dist, 100, generate_steps(2))))

# Part 2
print(len(get_cheats(dist, 100, generate_steps(20))))
