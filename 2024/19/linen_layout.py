#!/usr/bin/python3

from functools import cache

with open('input', 'r', encoding='utf-8') as input_file:
    towels = set(towel.strip() for towel in input_file.readline().split(', '))

    designs = [ line.strip() for line in input_file.readlines() if line.strip() ]

@cache
def count_combinations(design: str):
    ret = 0
    for towel in towels:
        if design.startswith(towel):
            right = design[len(towel):]
            if len(right) == 0:
                ret += 1
            else:
                ret += count_combinations(right)

    return ret

combinations = [ count_combinations(design) for design in designs ]

# Part 1
print(sum(comb > 0 for comb in combinations))

# Part 2
print(sum(comb for comb in combinations))

