#!/usr/bin/python3

from collections import defaultdict


with open('input', 'r', encoding='utf-8') as input_file:
    secrets = [ int(line) for line in input_file ]

MASK = 0xFFFFFF

def rotate_secret(secret, n, sequences: dict[tuple, int]):
    last = secret % 10
    current_var = 0
    done_sequence = set()

    history = []
    for _ in range(n):
        secret ^= (secret & MASK) << 6
        secret ^= (secret & MASK) >> 5
        secret ^= (secret & MASK) << 11

        price = (secret & MASK) % 10

        current_var += price - last
        history.append(price - last)
        last = price

        if len(history) == 4:
            history_t = tuple(history)

            if current_var > 0 and history[3] > 0 and history_t not in done_sequence:
                sequences[history_t] += price
                done_sequence.add(history_t)

            current_var -= history.pop(0)

    return secret & MASK

sequences = defaultdict(int)

# Part 1
print(sum(rotate_secret(secret, 2000, sequences) for secret in secrets))

# Part 2
print(max(sequences.values()))
