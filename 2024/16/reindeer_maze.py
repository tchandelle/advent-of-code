#!/usr/bin/python3


from collections import defaultdict
from heapq import heappop, heappush
from math import inf

VEC = [
    +1+0j, # east
    +0+1j, # south
    -1+0j, # west
    +0-1j, # north
]

def count_tiles(prev, u):
    ret = set([ u[0] ])
    for v in prev[u]:
        ret |= count_tiles(prev, v)
    return ret


def navigate_maze(maze: dict[complex, str], start: complex, end: complex):
    # Counter used in the heap to avoid comparing complex
    c = 0

    dist = defaultdict(lambda: inf)
    prev = defaultdict(lambda: [])
    q = []

    u = (start, VEC[0])

    heappush(q, (0, c, u))
    dist[u] = 0

    while len(q):
        u_dist, _, u = heappop(q)

        u_pos, u_dir = u
        if u_pos == end:
            tiles = count_tiles(prev, (end, VEC[0])) | count_tiles(prev, (end, VEC[3]))

            return u_dist, len(tiles)

        v_pos = u_pos + u_dir
        v = (v_pos, u_dir)
        if maze[v_pos] != '#':
            alt = u_dist + 1
            if alt < dist[v]:
                dist[v] = alt
                c += 1
                heappush(q, (dist[v], c, v))
                prev[v] = [ u ]
            elif alt == dist[v]:
                prev[v].append(u)

        for vec in VEC:
            alt = u_dist + 1000
            v = (u_pos, vec)
            if vec != u_dir and alt < dist[v]:
                dist[v] = alt
                c += 1
                heappush(q, (dist[v], c, v))
                prev[v] = [ u ]
            elif alt == dist[v]:
                prev[v].append(u)


def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        maze = {
            complex(x, y): c
            for y, line in enumerate(input_file.readlines())
            for x, c in enumerate(line.strip())
        }

    # Part 1 & 2
    start_idx = list(maze.values()).index('S')
    end_idx = list(maze.values()).index('E')
    start = list(maze.keys())[start_idx]
    end = list(maze.keys())[end_idx]

    lowest_score, best_tiles = navigate_maze(maze, start, end)

    print(lowest_score)
    print(best_tiles)


if __name__ == "__main__":
    main()
