#!/usr/bin/python3

import re

PATTERN = r'.*X.(\d+), Y.(\d+)$'

def solve(machine):
    x = [machine[0][0], machine[1][0], machine[2][0]]
    y = [machine[0][1], machine[1][1], machine[2][1]]

    x = [i * machine[0][1] for i in x]
    y = [i * machine[0][0] for i in y]

    b = (x[2] - y[2]) / (x[1] - y[1])
    a = (x[2] - x[1] * b) / x[0]

    res = (int(a), int(b))

    return res if res == (a, b) else (0, 0)

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        lines = input_file.readlines()
        button_a = lines[0::4]
        button_b = lines[1::4]
        prize = lines[2::4]

        machines = []
        for i in range(len(prize)):
            a = re.match(PATTERN, button_a[i])
            b = re.match(PATTERN, button_b[i])
            p = re.match(PATTERN, prize[i])
            machines.append((
                (int(a[1]), int(a[2])),
                (int(b[1]), int(b[2])),
                (int(p[1]), int(p[2])),
            ))

    # Part 1
    print(sum(
        a * 3 + b
        for machine in machines
        for a, b in [solve(machine)]
    ))

    # Part 2
    machines = [(a, b, tuple(n + 10000000000000 for n in p)) for a, b, p in machines]
    print(sum(
        a * 3 + b
        for machine in machines
        for a, b in [solve(machine)]
    ))



if __name__ == "__main__":
    main()
