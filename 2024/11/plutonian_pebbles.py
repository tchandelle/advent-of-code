#!/usr/bin/python3

from collections import defaultdict


def blink(stones: dict[int, int]):
    ret = defaultdict(int)
    for num, cnt in stones.items():
        if (len_stone := len(str_stone := str(num))) % 2 == 0:
            mid = int(len_stone / 2)
            left, right = int(str_stone[:mid]), int(str_stone[mid:])

            ret[left] += cnt
            ret[right] += cnt
        else:
            num = 1 if num == 0 else num * 2024
            ret[num] += cnt

    return ret

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        # Expect one stone per number
        stones = { int(s): 1 for s in input_file.readline().split() }

    # Part 1 & 2
    for i in range(75):
        stones = blink(stones)
        match i + 1:
            case 25 | 75:
                print(sum(stones.values()))


if __name__ == "__main__":
    main()
