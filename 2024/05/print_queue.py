#!/usr/bin/python3

from functools import cmp_to_key
from math import floor

def middle_page(update):
    return int(update[floor(len(update) / 2)])

def get_broken_rule(update, rules):
    for i, p0 in enumerate(update):
        for p1 in update[i+1:]:
            if (p0, p1) not in rules:
                return (p0, p1)
    return None

def is_update_correct(update, rules):
    return get_broken_rule(update, rules) is None

def fix_update(update, rules):
    return sorted(update, key=cmp_to_key(lambda x, y: 0 if (x, y) in rules else -1 ))

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        lines = input_file.readlines()
        middle = lines.index('\n')

        page_ordering_rules = [
            tuple(l.strip().split('|'))
            for l in lines[:middle]
        ]
        updates = [
            l.strip().split(',')
            for l in lines[middle+1:]
        ]

    # Part 1
    print(sum(
        middle_page(update)
            if is_update_correct(update, page_ordering_rules)
            else 0
        for update in updates
    ))

    # Part 2
    print(sum(
        middle_page(fix_update(update, page_ordering_rules))
            if not is_update_correct(update, page_ordering_rules)
            else 0
        for update in updates
    ))


if __name__ == "__main__":
    main()
