#!/usr/bin/python3

import functools
import operator
import os
import re

CD = '$ cd '
LS = '$ ls'
SIZE = '_size'

def build_fs(lines):
    fs = {'/': { SIZE: 0 }}
    cwd = fs
    pwd = ''

    for line in lines:
        if line.startswith(CD):
            listing = False
            target = line[len(CD):]
            cwd = cwd[target]
            pwd = os.path.normpath(os.path.join(pwd, target))
        elif line.startswith(LS):
            listing = True
        elif listing:
            m = re.match(r'(dir|\d+) (.*)', line)
            match m[1]:
                case 'dir':
                    item = { '..': cwd, SIZE: 0 }
                case _:
                    item = int(m[1])
                    recursive_add_size(cwd, int(m[1]))
            cwd[m[2]] = item
            fs[os.path.join(pwd, m[2])] = item

    return fs

def recursive_add_size(cwd, size):
    while SIZE in cwd:
        cwd[SIZE] += size
        if '..' not in cwd:
            return
        cwd = cwd['..']

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        fs = build_fs([line.strip() for line in input_file])

    directories = filter(lambda item : isinstance(item, dict), fs.values())
    sizes = [item[SIZE] for item in directories]

    print(functools.reduce(operator.add, filter(lambda size : size < 100000, sizes), 0))

    available = 70000000 - fs['/'][SIZE]
    required = 30000000 - available
    print(next(s for s in sorted(sizes) if s > required))

if __name__ == "__main__":
    main()
