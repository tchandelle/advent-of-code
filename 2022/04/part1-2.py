#!/usr/bin/python3

def deserialize(s):
    return tuple(map(
        lambda s : range(*map(int, s.split('-'))),
        s.strip().split(',')
    ))

def fully_contains(t):
    a, b = t
    return ((a.start >= b.start and a.stop <= b.stop) or
        (b.start >= a.start and b.stop <= a.stop))

def overlap(t):
    a, b = t
    return (
        (a.start >= b.start and a.start <= b.stop) or
        (a.stop >= b.start and a.stop <= b.stop) or
        (b.start >= a.start and b.start <= a.stop) or
        (b.stop >= a.start and b.stop <= a.stop)
    )

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        pairs = list(map(deserialize, input_file))

        contains = filter(lambda c : c, map(fully_contains, pairs))
        print(len(list(contains)))

        overlapping = filter(lambda c : c, map(overlap, pairs))
        print(len(list(overlapping)))

if __name__ == "__main__":
    main()
