#!/usr/bin/python3

def find_marker(data, length = 4):
    i = 0
    for letters in zip(*[ data[idx:] for idx in range(length) ]):
        if len(dict.fromkeys(letters)) == length:
            return i + length
        i += 1

assert find_marker('mjqjpqmgbljsphdztnvjfqwrcgsmlb') == 7
assert find_marker('bvwbjplbgvbhsrlpgdmjqwftvncz') == 5
assert find_marker('nppdvjthqldpwncqszvftbrmjlhg') == 6
assert find_marker('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg') == 10
assert find_marker('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw') == 11

assert find_marker('mjqjpqmgbljsphdztnvjfqwrcgsmlb', 14) == 19
assert find_marker('bvwbjplbgvbhsrlpgdmjqwftvncz', 14) == 23
assert find_marker('nppdvjthqldpwncqszvftbrmjlhg', 14) == 23
assert find_marker('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 14) == 29
assert find_marker('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', 14) == 26

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        print(find_marker(input_file.read()))
    with open('input', 'r', encoding='utf-8') as input_file:
        print(find_marker(input_file.read(), 14))

if __name__ == "__main__":
    main()
