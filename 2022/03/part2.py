#!/usr/bin/python3

import functools
import operator

def get_common_letters(pair):
    for letter in pair[0]:
        if letter in pair[1] and letter in pair[2]:
            return letter

def get_priority(letter):
    if letter.islower():
        return ord(letter) - ord('a') + 1
    return ord(letter) - ord('A') + 26 + 1

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        data = input_file.readlines()
        by_group = [ (data[i:i+3]) for i in range(0, len(data), 3) ]

        common_letters = map(get_common_letters, by_group)

        priorities = map(get_priority, common_letters)

        print(functools.reduce(operator.add, priorities, 0))

if __name__ == "__main__":
    main()
