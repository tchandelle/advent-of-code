#!/usr/bin/python3

import functools
import operator

def cut_in_half(s):
    s = s.strip()
    return s[:len(s)//2], s[len(s)//2:]

def get_common_letters(pair):
    for letter in pair[0]:
        if letter in pair[1]:
            return letter

def get_priority(letter):
    if letter.islower():
        return ord(letter) - ord('a') + 1
    return ord(letter) - ord('A') + 26 + 1

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        pairs = map(cut_in_half, input_file)
        common_letters = map(get_common_letters, pairs)
        priorities = map(get_priority, common_letters)

        print(functools.reduce(operator.add, priorities, 0))

if __name__ == "__main__":
    main()
