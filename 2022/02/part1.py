#!/usr/bin/python3

from enum import Enum
import functools
import operator
import re

class Choice(Enum):
    ROCK, PAPER, SCISSORS = range(1, 4)
    A, B, C = range(1, 4)
    X, Y, Z = range(1, 4)

LOSE = 0
DRAW = 3
WIN = 6

def get_winner(them, you):
    if you == them:
        return DRAW

    match you:
        case Choice.ROCK:
            return LOSE if them == Choice.PAPER else WIN
        case Choice.PAPER:
            return LOSE if them == Choice.SCISSORS else WIN
        case Choice.SCISSORS:
            return LOSE if them == Choice.ROCK else WIN

def get_round_score(them, you):
    return you.value + get_winner(them, you)

def get_round_score_from_str(data):
    m = re.match(r'(A|B|C) (X|Y|Z)', data)
    if m is None:
        return 0

    them = Choice[m[1]]
    you = Choice[m[2]]

    return get_round_score(them, you)


assert get_round_score(Choice.A, Choice.Y) == 8
assert get_round_score(Choice.B, Choice.X) == 1
assert get_round_score(Choice.C, Choice.Z) == 6

with open('./input', 'r', encoding='utf-8') as input_file:
    inputs = input_file.readlines()

round_scores = map(get_round_score_from_str, inputs)

print(functools.reduce(operator.add, round_scores, 0))
