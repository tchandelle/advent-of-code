#!/usr/bin/python3

from enum import Enum
import functools
import operator
import re

class Choice(Enum):
    ROCK, PAPER, SCISSORS = range(1, 4)
    A, B, C = range(1, 4)

class ResultNeeded(Enum):
    LOSE, DRAW, WIN = range(1, 4)
    X, Y, Z = range(1, 4)

LOSE = 0
DRAW = 3
WIN = 6

def get_required_play(them, result):
    match result:
        case ResultNeeded.DRAW:
            return them
        case ResultNeeded.WIN:
            match them:
                case Choice.ROCK:
                    return Choice.PAPER
                case Choice.PAPER:
                    return Choice.SCISSORS
                case Choice.SCISSORS:
                    return Choice.ROCK
        case ResultNeeded.LOSE:
            match them:
                case Choice.ROCK:
                    return Choice.SCISSORS
                case Choice.PAPER:
                    return Choice.ROCK
                case Choice.SCISSORS:
                    return Choice.PAPER

def get_winner(them, you):
    if you == them:
        return DRAW

    match you:
        case Choice.ROCK:
            return LOSE if them == Choice.PAPER else WIN
        case Choice.PAPER:
            return LOSE if them == Choice.SCISSORS else WIN
        case Choice.SCISSORS:
            return LOSE if them == Choice.ROCK else WIN

def get_round_score(them, result):
    you = get_required_play(them, result)

    return you.value + get_winner(them, you)

def get_round_score_from_str(data):
    m = re.match(r'(A|B|C) (X|Y|Z)', data)
    if m is None:
        return 0

    them = Choice[m[1]]
    result = ResultNeeded[m[2]]

    return get_round_score(them, result)


assert get_round_score(Choice.A, ResultNeeded.Y) == 4
assert get_round_score(Choice.B, ResultNeeded.X) == 1
assert get_round_score(Choice.C, ResultNeeded.Z) == 7

with open('./input', 'r', encoding='utf-8') as input_file:
    inputs = input_file.readlines()

round_scores = map(get_round_score_from_str, inputs)

print(functools.reduce(operator.add, round_scores, 0))
