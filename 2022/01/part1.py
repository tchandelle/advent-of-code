#!/usr/bin/python3
import functools

with open('./input', 'r', encoding='utf-8') as input_file:
    inputs = input_file.readlines()

def add_calories(elves, calories):
    if calories.strip() == '':
        return elves + [0]

    current_elf = elves.pop()
    current_elf += int(calories)

    return elves + [current_elf]

calories_by_elves = functools.reduce(add_calories, inputs, [0])
calories_by_elves.sort(reverse=True)

a = calories_by_elves[0]
b = calories_by_elves[1]
c = calories_by_elves[2]

print(a)
print(b)
print(c)
print('-----')
print(a + b + c)
