#!/usr/bin/python3

import re

def parse_stacks(data):
    stacks = [ [] for i in range((len(data.pop()) + 1 + 4) // 4) ]
    for line in reversed(data):
        i = 0
        for idx in range(1, len(line), 4):
            if line[idx] != ' ':
                stacks[i].append(line[idx])
            i += 1

    return stacks

def apply_cmd_9000(stacks, cmd):
    m = re.match(r'move (\d+) from (\d+) to (\d+)', cmd)
    count = int(m[1])
    src = int(m[2]) - 1
    dest = int(m[3]) - 1

    for _ in range(count):
        stacks[dest].append(stacks[src].pop())

def apply_cmd_9001(stacks, cmd):
    m = re.match(r'move (\d+) from (\d+) to (\d+)', cmd)
    count = int(m[1])
    src = int(m[2]) - 1
    dest = int(m[3]) - 1

    boxes = [ stacks[src].pop() for _ in range(count) ]
    stacks[dest].extend(reversed(boxes))

def main():
    with open('input', 'r', encoding='utf-8') as input_file:
        data = [ line.rstrip() for line in input_file ]
        separator = data.index('')
        stacks1 = parse_stacks(data[:separator])
        stacks2 = parse_stacks(data[:separator])
        cmds = data[separator+1:]

    for cmd in cmds:
        apply_cmd_9000(stacks1, cmd)
    print(''.join([ boxes[-1] for boxes in stacks1 ]))

    for cmd in cmds:
        apply_cmd_9001(stacks2, cmd)
    print(''.join([ boxes[-1] for boxes in stacks2 ]))


if __name__ == "__main__":
    main()
